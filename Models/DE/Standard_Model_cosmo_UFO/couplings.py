# This file was automatically created by FeynRules 2.0.33
# Mathematica version: 7.0 for Mac OS X x86 (64-bit) (February 19, 2009)
# Date: Tue 19 Jan 2016 17:07:07


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-4*aH*complex(0,1)',
                order = {'HEFT':1})

GC_2 = Coupling(name = 'GC_2',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-ee**2/(2.*cw)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '(ee**2*complex(0,1))/(2.*cw)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = 'ee**2/(2.*cw)',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-4*aH*G',
                 order = {'HEFT':1,'QCD':1})

GC_14 = Coupling(name = 'GC_14',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '4*aH*complex(0,1)*G**2',
                 order = {'HEFT':1,'QCD':2})

GC_16 = Coupling(name = 'GC_16',
                 value = 'I1a11',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'I1a12',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'I1a13',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'I1a21',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = 'I1a22',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = 'I1a23',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = 'I1a31',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'I1a32',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = 'I1a33',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '-I2a11',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-I2a12',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-I2a13',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-I2a21',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '-I2a22',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-I2a23',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '-I2a31',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '-I2a32',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '-I2a33',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = 'I3a11',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = 'I3a12',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = 'I3a13',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = 'I3a21',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = 'I3a22',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = 'I3a23',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = 'I3a31',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = 'I3a32',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = 'I3a33',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-I4a11',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-I4a12',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-I4a13',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-I4a21',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-I4a22',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-I4a23',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-I4a31',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '-I4a32',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-I4a33',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '-12*c12a*complex(0,1)*invMscale',
                 order = {'NEW':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '-48*c12b*complex(0,1)*invMscale**2',
                 order = {'NEW':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '12*c12c*complex(0,1)*invMscale**3',
                 order = {'NEW':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '4*c7*complex(0,1)*invMscale**3',
                 order = {'NEW':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '-4*c1*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-6*c1*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-8*c1*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '48*c12d*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_60 = Coupling(name = 'GC_60',
                 value = 'c2*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '2*c2*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-4*c2*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '48*c6a*complex(0,1)*invMscale**4',
                 order = {'NEW':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-2*c1*ee*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-4*c1*ee*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '4*c1*ee*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '-6*c1*ee*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(2*c2*ee*complex(0,1)*invMscale**4)/3.',
                 order = {'NEW':1,'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(-4*c2*ee*complex(0,1)*invMscale**4)/3.',
                 order = {'NEW':1,'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '2*c2*ee*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '8*c1*ee**2*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '-4*c2*ee**2*complex(0,1)*invMscale**4',
                 order = {'NEW':1,'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '(-2*c1*ee**2*invMscale**4)/cw',
                 order = {'NEW':1,'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '(2*c1*ee**2*complex(0,1)*invMscale**4)/cw',
                 order = {'NEW':1,'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '(2*c1*ee**2*invMscale**4)/cw',
                 order = {'NEW':1,'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '-((c2*ee**2*invMscale**4)/cw)',
                 order = {'NEW':1,'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '-((c2*ee**2*complex(0,1)*invMscale**4)/cw)',
                 order = {'NEW':1,'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = '(c2*ee**2*invMscale**4)/cw',
                 order = {'NEW':1,'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '12*c1*complex(0,1)*G*invMscale**4',
                 order = {'NEW':1,'QCD':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-2*c2*complex(0,1)*G*invMscale**4',
                 order = {'NEW':1,'QCD':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '4*c2*G*invMscale**4',
                 order = {'NEW':1,'QCD':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '4*c2*complex(0,1)*G**2*invMscale**4',
                 order = {'NEW':1,'QCD':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '16*c8*complex(0,1)*invMscale**6',
                 order = {'NEW':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '16*c3a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '24*c3a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '32*c3a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '-2*c4a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '-4*c4a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '8*c4a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '2*c5a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '8*c5a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-16*c5a*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '-1440*c6b*complex(0,1)*invMscale**8',
                 order = {'NEW':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '8*c3a*ee*complex(0,1)*invMscale**8',
                 order = {'NEW':1,'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-16*c3a*ee*complex(0,1)*invMscale**8',
                 order = {'NEW':1,'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '16*c3a*ee*complex(0,1)*invMscale**8',
                 order = {'NEW':1,'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '24*c3a*ee*complex(0,1)*invMscale**8',
                 order = {'NEW':1,'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(-4*c4a*ee*complex(0,1)*invMscale**8)/3.',
                 order = {'NEW':1,'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(8*c4a*ee*complex(0,1)*invMscale**8)/3.',
                 order = {'NEW':1,'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '-4*c4a*ee*complex(0,1)*invMscale**8',
                  order = {'NEW':1,'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(4*c5a*ee*complex(0,1)*invMscale**8)/3.',
                  order = {'NEW':1,'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(-8*c5a*ee*complex(0,1)*invMscale**8)/3.',
                  order = {'NEW':1,'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '4*c5a*ee*complex(0,1)*invMscale**8',
                  order = {'NEW':1,'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '-32*c3a*ee**2*complex(0,1)*invMscale**8',
                  order = {'NEW':1,'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '8*c4a*ee**2*complex(0,1)*invMscale**8',
                  order = {'NEW':1,'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '(-8*c3a*ee**2*invMscale**8)/cw',
                  order = {'NEW':1,'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '(-8*c3a*ee**2*complex(0,1)*invMscale**8)/cw',
                  order = {'NEW':1,'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = '(8*c3a*ee**2*invMscale**8)/cw',
                  order = {'NEW':1,'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = '(-2*c4a*ee**2*invMscale**8)/cw',
                  order = {'NEW':1,'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '(2*c4a*ee**2*complex(0,1)*invMscale**8)/cw',
                  order = {'NEW':1,'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(2*c4a*ee**2*invMscale**8)/cw',
                  order = {'NEW':1,'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '-48*c3a*complex(0,1)*G*invMscale**8',
                  order = {'NEW':1,'QCD':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '-8*c4a*G*invMscale**8',
                  order = {'NEW':1,'QCD':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '4*c4a*complex(0,1)*G*invMscale**8',
                  order = {'NEW':1,'QCD':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '-6*c5a*G*invMscale**8',
                  order = {'NEW':1,'QCD':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-4*c5a*complex(0,1)*G*invMscale**8',
                  order = {'NEW':1,'QCD':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-8*c4a*complex(0,1)*G**2*invMscale**8',
                  order = {'NEW':1,'QCD':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '-6*c5a*complex(0,1)*G**2*invMscale**8',
                  order = {'NEW':1,'QCD':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '24*c9*complex(0,1)*invMscale**9',
                  order = {'NEW':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '2*c10a*complex(0,1)*invNscale',
                  order = {'NEW':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '3*c10a*complex(0,1)*invNscale',
                  order = {'NEW':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '4*c10a*complex(0,1)*invNscale',
                  order = {'NEW':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '240*c11a*complex(0,1)*invNscale',
                  order = {'NEW':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '2*c10a*ee*complex(0,1)*invNscale',
                  order = {'NEW':1,'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '-4*c10a*ee*complex(0,1)*invNscale',
                  order = {'NEW':1,'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '6*c10a*ee*complex(0,1)*invNscale',
                  order = {'NEW':1,'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '-4*c10a*ee**2*complex(0,1)*invNscale',
                  order = {'NEW':1,'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = '-((c10a*ee**2*invNscale)/cw)',
                  order = {'NEW':1,'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '-((c10a*ee**2*complex(0,1)*invNscale)/cw)',
                  order = {'NEW':1,'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(c10a*ee**2*invNscale)/cw',
                  order = {'NEW':1,'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '-6*c10a*complex(0,1)*G*invNscale',
                  order = {'NEW':1,'QCD':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '4*c10b*complex(0,1)*invNscale**2',
                  order = {'NEW':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '6*c10b*complex(0,1)*invNscale**2',
                  order = {'NEW':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '8*c10b*complex(0,1)*invNscale**2',
                  order = {'NEW':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '1440*c11b*complex(0,1)*invNscale**2',
                  order = {'NEW':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '2*c10b*ee*complex(0,1)*invNscale**2',
                  order = {'NEW':1,'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-4*c10b*ee*complex(0,1)*invNscale**2',
                  order = {'NEW':1,'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '4*c10b*ee*complex(0,1)*invNscale**2',
                  order = {'NEW':1,'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '6*c10b*ee*complex(0,1)*invNscale**2',
                  order = {'NEW':1,'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-8*c10b*ee**2*complex(0,1)*invNscale**2',
                  order = {'NEW':1,'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = '(-2*c10b*ee**2*invNscale**2)/cw',
                  order = {'NEW':1,'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = '(-2*c10b*ee**2*complex(0,1)*invNscale**2)/cw',
                  order = {'NEW':1,'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '(2*c10b*ee**2*invNscale**2)/cw',
                  order = {'NEW':1,'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = '-12*c10b*complex(0,1)*G*invNscale**2',
                  order = {'NEW':1,'QCD':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '12*c10c*complex(0,1)*invNscale**3',
                  order = {'NEW':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '18*c10c*complex(0,1)*invNscale**3',
                  order = {'NEW':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '24*c10c*complex(0,1)*invNscale**3',
                  order = {'NEW':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '10080*c11c*complex(0,1)*invNscale**3',
                  order = {'NEW':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '12*c10c*ee*complex(0,1)*invNscale**3',
                  order = {'NEW':1,'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-24*c10c*ee*complex(0,1)*invNscale**3',
                  order = {'NEW':1,'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '36*c10c*ee*complex(0,1)*invNscale**3',
                  order = {'NEW':1,'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-24*c10c*ee**2*complex(0,1)*invNscale**3',
                  order = {'NEW':1,'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = '(-6*c10c*ee**2*invNscale**3)/cw',
                  order = {'NEW':1,'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '(-6*c10c*ee**2*complex(0,1)*invNscale**3)/cw',
                  order = {'NEW':1,'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = '(6*c10c*ee**2*invNscale**3)/cw',
                  order = {'NEW':1,'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '-36*c10c*complex(0,1)*G*invNscale**3',
                  order = {'NEW':1,'QCD':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '48*c10d*complex(0,1)*invNscale**4',
                  order = {'NEW':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '72*c10d*complex(0,1)*invNscale**4',
                  order = {'NEW':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '96*c10d*complex(0,1)*invNscale**4',
                  order = {'NEW':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '80640*c11d*complex(0,1)*invNscale**4',
                  order = {'NEW':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '24*c10d*ee*complex(0,1)*invNscale**4',
                  order = {'NEW':1,'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-48*c10d*ee*complex(0,1)*invNscale**4',
                  order = {'NEW':1,'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '48*c10d*ee*complex(0,1)*invNscale**4',
                  order = {'NEW':1,'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '72*c10d*ee*complex(0,1)*invNscale**4',
                  order = {'NEW':1,'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-96*c10d*ee**2*complex(0,1)*invNscale**4',
                  order = {'NEW':1,'QED':2})

GC_166 = Coupling(name = 'GC_166',
                  value = '(-24*c10d*ee**2*invNscale**4)/cw',
                  order = {'NEW':1,'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '(-24*c10d*ee**2*complex(0,1)*invNscale**4)/cw',
                  order = {'NEW':1,'QED':2})

GC_168 = Coupling(name = 'GC_168',
                  value = '(24*c10d*ee**2*invNscale**4)/cw',
                  order = {'NEW':1,'QED':2})

GC_169 = Coupling(name = 'GC_169',
                  value = '-144*c10d*complex(0,1)*G*invNscale**4',
                  order = {'NEW':1,'QCD':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-2*complex(0,1)*lam',
                  order = {'QED':2})

GC_171 = Coupling(name = 'GC_171',
                  value = '-4*complex(0,1)*lam',
                  order = {'QED':2})

GC_172 = Coupling(name = 'GC_172',
                  value = '-6*complex(0,1)*lam',
                  order = {'QED':2})

GC_173 = Coupling(name = 'GC_173',
                  value = '8*c10a*complex(0,1)*invNscale*lam',
                  order = {'NEW':1,'QED':2})

GC_174 = Coupling(name = 'GC_174',
                  value = '16*c10a*complex(0,1)*invNscale*lam',
                  order = {'NEW':1,'QED':2})

GC_175 = Coupling(name = 'GC_175',
                  value = '24*c10a*complex(0,1)*invNscale*lam',
                  order = {'NEW':1,'QED':2})

GC_176 = Coupling(name = 'GC_176',
                  value = '16*c10b*complex(0,1)*invNscale**2*lam',
                  order = {'NEW':1,'QED':2})

GC_177 = Coupling(name = 'GC_177',
                  value = '32*c10b*complex(0,1)*invNscale**2*lam',
                  order = {'NEW':1,'QED':2})

GC_178 = Coupling(name = 'GC_178',
                  value = '48*c10b*complex(0,1)*invNscale**2*lam',
                  order = {'NEW':1,'QED':2})

GC_179 = Coupling(name = 'GC_179',
                  value = '48*c10c*complex(0,1)*invNscale**3*lam',
                  order = {'NEW':1,'QED':2})

GC_180 = Coupling(name = 'GC_180',
                  value = '96*c10c*complex(0,1)*invNscale**3*lam',
                  order = {'NEW':1,'QED':2})

GC_181 = Coupling(name = 'GC_181',
                  value = '144*c10c*complex(0,1)*invNscale**3*lam',
                  order = {'NEW':1,'QED':2})

GC_182 = Coupling(name = 'GC_182',
                  value = '192*c10d*complex(0,1)*invNscale**4*lam',
                  order = {'NEW':1,'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = '384*c10d*complex(0,1)*invNscale**4*lam',
                  order = {'NEW':1,'QED':2})

GC_184 = Coupling(name = 'GC_184',
                  value = '576*c10d*complex(0,1)*invNscale**4*lam',
                  order = {'NEW':1,'QED':2})

GC_185 = Coupling(name = 'GC_185',
                  value = '-16*c1*complex(0,1)*invMscale**4*lam - 4*c2*complex(0,1)*invMscale**4*lam',
                  order = {'NEW':1,'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = '-32*c1*complex(0,1)*invMscale**4*lam - 8*c2*complex(0,1)*invMscale**4*lam',
                  order = {'NEW':1,'QED':2})

GC_187 = Coupling(name = 'GC_187',
                  value = '-48*c1*complex(0,1)*invMscale**4*lam - 12*c2*complex(0,1)*invMscale**4*lam',
                  order = {'NEW':1,'QED':2})

GC_188 = Coupling(name = 'GC_188',
                  value = '64*c3a*complex(0,1)*invMscale**8*lam + 16*c4a*complex(0,1)*invMscale**8*lam',
                  order = {'NEW':1,'QED':2})

GC_189 = Coupling(name = 'GC_189',
                  value = '128*c3a*complex(0,1)*invMscale**8*lam + 32*c4a*complex(0,1)*invMscale**8*lam',
                  order = {'NEW':1,'QED':2})

GC_190 = Coupling(name = 'GC_190',
                  value = '192*c3a*complex(0,1)*invMscale**8*lam + 48*c4a*complex(0,1)*invMscale**8*lam',
                  order = {'NEW':1,'QED':2})

GC_191 = Coupling(name = 'GC_191',
                  value = '8*c10a*complex(0,1)*invNscale*MB',
                  order = {'NEW':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '16*c10b*complex(0,1)*invNscale**2*MB',
                  order = {'NEW':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '48*c10c*complex(0,1)*invNscale**3*MB',
                  order = {'NEW':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '192*c10d*complex(0,1)*invNscale**4*MB',
                  order = {'NEW':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-16*c1*complex(0,1)*invMscale**4*MB - 4*c2*complex(0,1)*invMscale**4*MB',
                  order = {'NEW':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '64*c3a*complex(0,1)*invMscale**8*MB + 16*c4a*complex(0,1)*invMscale**8*MB - 16*c5a*complex(0,1)*invMscale**8*MB',
                  order = {'NEW':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '8*c10a*complex(0,1)*invNscale*MT',
                  order = {'NEW':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '16*c10b*complex(0,1)*invNscale**2*MT',
                  order = {'NEW':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '48*c10c*complex(0,1)*invNscale**3*MT',
                  order = {'NEW':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '192*c10d*complex(0,1)*invNscale**4*MT',
                  order = {'NEW':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-16*c1*complex(0,1)*invMscale**4*MT - 4*c2*complex(0,1)*invMscale**4*MT',
                  order = {'NEW':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '64*c3a*complex(0,1)*invMscale**8*MT + 16*c4a*complex(0,1)*invMscale**8*MT - 16*c5a*complex(0,1)*invMscale**8*MT',
                  order = {'NEW':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '8*c10a*complex(0,1)*invNscale*MTA',
                  order = {'NEW':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '16*c10b*complex(0,1)*invNscale**2*MTA',
                  order = {'NEW':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '48*c10c*complex(0,1)*invNscale**3*MTA',
                  order = {'NEW':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '192*c10d*complex(0,1)*invNscale**4*MTA',
                  order = {'NEW':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-16*c1*complex(0,1)*invMscale**4*MTA - 4*c2*complex(0,1)*invMscale**4*MTA',
                  order = {'NEW':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '64*c3a*complex(0,1)*invMscale**8*MTA + 16*c4a*complex(0,1)*invMscale**8*MTA - 16*c5a*complex(0,1)*invMscale**8*MTA',
                  order = {'NEW':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '16*c5a*complex(0,1)*invMscale**8*MW**2',
                  order = {'NEW':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '16*c5a*complex(0,1)*invMscale**8*MZ**2',
                  order = {'NEW':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '(ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_212 = Coupling(name = 'GC_212',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_213 = Coupling(name = 'GC_213',
                  value = '(cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_214 = Coupling(name = 'GC_214',
                  value = '(2*c1*ee**2*complex(0,1)*invMscale**4)/sw**2',
                  order = {'NEW':1,'QED':2})

GC_215 = Coupling(name = 'GC_215',
                  value = '-((c2*ee**2*complex(0,1)*invMscale**4)/sw**2)',
                  order = {'NEW':1,'QED':2})

GC_216 = Coupling(name = 'GC_216',
                  value = '(-8*c3a*ee**2*complex(0,1)*invMscale**8)/sw**2',
                  order = {'NEW':1,'QED':2})

GC_217 = Coupling(name = 'GC_217',
                  value = '(2*c4a*ee**2*complex(0,1)*invMscale**8)/sw**2',
                  order = {'NEW':1,'QED':2})

GC_218 = Coupling(name = 'GC_218',
                  value = '-((c10a*ee**2*complex(0,1)*invNscale)/sw**2)',
                  order = {'NEW':1,'QED':2})

GC_219 = Coupling(name = 'GC_219',
                  value = '(-2*c10b*ee**2*complex(0,1)*invNscale**2)/sw**2',
                  order = {'NEW':1,'QED':2})

GC_220 = Coupling(name = 'GC_220',
                  value = '(-6*c10c*ee**2*complex(0,1)*invNscale**3)/sw**2',
                  order = {'NEW':1,'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = '(-24*c10d*ee**2*complex(0,1)*invNscale**4)/sw**2',
                  order = {'NEW':1,'QED':2})

GC_222 = Coupling(name = 'GC_222',
                  value = '-ee/(2.*sw)',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '-(ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '(ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-(cw*ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '(cw*ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '-ee**2/(2.*sw)',
                  order = {'QED':2})

GC_240 = Coupling(name = 'GC_240',
                  value = '-(ee**2*complex(0,1))/(2.*sw)',
                  order = {'QED':2})

GC_241 = Coupling(name = 'GC_241',
                  value = 'ee**2/(2.*sw)',
                  order = {'QED':2})

GC_242 = Coupling(name = 'GC_242',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_243 = Coupling(name = 'GC_243',
                  value = '(-2*c1*ee*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '(-2*c1*ee*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '(2*c1*ee*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '-((c2*ee*complex(0,1)*invMscale**4)/sw)',
                  order = {'NEW':1,'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '(c2*ee*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '(c2*ee*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '(6*c1*CKM1x1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '-((c2*CKM1x1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(6*c1*CKM1x2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '-((c2*CKM1x2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(6*c1*CKM1x3*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '-((c2*CKM1x3*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(6*c1*CKM2x1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '-((c2*CKM2x1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(6*c1*CKM2x2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '-((c2*CKM2x2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '(6*c1*CKM2x3*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '-((c2*CKM2x3*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '(6*c1*CKM3x1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '-((c2*CKM3x1*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '(6*c1*CKM3x2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '-((c2*CKM3x2*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(6*c1*CKM3x3*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '-((c2*CKM3x3*ee*complex(0,1)*invMscale**4*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '(-6*c1*cw*ee*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '(6*c1*cw*ee*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-((c2*cw*ee*complex(0,1)*invMscale**4)/sw)',
                  order = {'NEW':1,'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '(c2*cw*ee*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(-2*c1*ee**2*invMscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_274 = Coupling(name = 'GC_274',
                  value = '(-2*c1*ee**2*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = '(2*c1*ee**2*invMscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_276 = Coupling(name = 'GC_276',
                  value = '-((c2*ee**2*invMscale**4)/sw)',
                  order = {'NEW':1,'QED':2})

GC_277 = Coupling(name = 'GC_277',
                  value = '(c2*ee**2*complex(0,1)*invMscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_278 = Coupling(name = 'GC_278',
                  value = '(c2*ee**2*invMscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_279 = Coupling(name = 'GC_279',
                  value = '(-8*c3a*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '(8*c3a*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '(8*c3a*ee*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '(-2*c4a*ee*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '(-2*c4a*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '(-24*c3a*CKM1x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '(2*c4a*CKM1x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '(-2*c5a*CKM1x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '(-24*c3a*CKM1x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '(2*c4a*CKM1x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '(-2*c5a*CKM1x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(-24*c3a*CKM1x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_295 = Coupling(name = 'GC_295',
                  value = '(2*c4a*CKM1x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '(-2*c5a*CKM1x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '(-24*c3a*CKM2x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '(2*c4a*CKM2x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_299 = Coupling(name = 'GC_299',
                  value = '(-2*c5a*CKM2x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '(-24*c3a*CKM2x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '(2*c4a*CKM2x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '(-2*c5a*CKM2x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(-24*c3a*CKM2x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '(2*c4a*CKM2x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '(-2*c5a*CKM2x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '(-24*c3a*CKM3x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '(2*c4a*CKM3x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '(-2*c5a*CKM3x1*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '(-24*c3a*CKM3x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '(2*c4a*CKM3x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '(-2*c5a*CKM3x2*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(-24*c3a*CKM3x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '(2*c4a*CKM3x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '(-2*c5a*CKM3x3*ee*complex(0,1)*invMscale**8*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '(-24*c3a*cw*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '(24*c3a*cw*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '(-2*c4a*cw*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '(2*c4a*cw*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '(-2*c5a*cw*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '(2*c5a*cw*ee*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '(-8*c3a*ee**2*invMscale**8)/sw',
                  order = {'NEW':1,'QED':2})

GC_322 = Coupling(name = 'GC_322',
                  value = '(8*c3a*ee**2*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = '(8*c3a*ee**2*invMscale**8)/sw',
                  order = {'NEW':1,'QED':2})

GC_324 = Coupling(name = 'GC_324',
                  value = '(-2*c4a*ee**2*invMscale**8)/sw',
                  order = {'NEW':1,'QED':2})

GC_325 = Coupling(name = 'GC_325',
                  value = '(-2*c4a*ee**2*complex(0,1)*invMscale**8)/sw',
                  order = {'NEW':1,'QED':2})

GC_326 = Coupling(name = 'GC_326',
                  value = '(2*c4a*ee**2*invMscale**8)/sw',
                  order = {'NEW':1,'QED':2})

GC_327 = Coupling(name = 'GC_327',
                  value = '-((c10a*ee*complex(0,1)*invNscale)/sw)',
                  order = {'NEW':1,'QED':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '(c10a*ee*complex(0,1)*invNscale)/sw',
                  order = {'NEW':1,'QED':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '(c10a*ee*invNscale)/sw',
                  order = {'NEW':1,'QED':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '(-3*c10a*CKM1x1*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '(-3*c10a*CKM1x2*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(-3*c10a*CKM1x3*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '(-3*c10a*CKM2x1*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '(-3*c10a*CKM2x2*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '(-3*c10a*CKM2x3*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '(-3*c10a*CKM3x1*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(-3*c10a*CKM3x2*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '(-3*c10a*CKM3x3*ee*complex(0,1)*invNscale*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '(-3*c10a*cw*ee*complex(0,1)*invNscale)/sw',
                  order = {'NEW':1,'QED':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '(3*c10a*cw*ee*complex(0,1)*invNscale)/sw',
                  order = {'NEW':1,'QED':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '-((c10a*ee**2*invNscale)/sw)',
                  order = {'NEW':1,'QED':2})

GC_343 = Coupling(name = 'GC_343',
                  value = '(c10a*ee**2*complex(0,1)*invNscale)/sw',
                  order = {'NEW':1,'QED':2})

GC_344 = Coupling(name = 'GC_344',
                  value = '(c10a*ee**2*invNscale)/sw',
                  order = {'NEW':1,'QED':2})

GC_345 = Coupling(name = 'GC_345',
                  value = '(-2*c10b*ee*complex(0,1)*invNscale**2)/sw',
                  order = {'NEW':1,'QED':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '(2*c10b*ee*complex(0,1)*invNscale**2)/sw',
                  order = {'NEW':1,'QED':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '(2*c10b*ee*invNscale**2)/sw',
                  order = {'NEW':1,'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '(-6*c10b*CKM1x1*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '(-6*c10b*CKM1x2*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '(-6*c10b*CKM1x3*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '(-6*c10b*CKM2x1*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(-6*c10b*CKM2x2*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '(-6*c10b*CKM2x3*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(-6*c10b*CKM3x1*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '(-6*c10b*CKM3x2*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(-6*c10b*CKM3x3*ee*complex(0,1)*invNscale**2*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '(-6*c10b*cw*ee*complex(0,1)*invNscale**2)/sw',
                  order = {'NEW':1,'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(6*c10b*cw*ee*complex(0,1)*invNscale**2)/sw',
                  order = {'NEW':1,'QED':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(-2*c10b*ee**2*invNscale**2)/sw',
                  order = {'NEW':1,'QED':2})

GC_361 = Coupling(name = 'GC_361',
                  value = '(2*c10b*ee**2*complex(0,1)*invNscale**2)/sw',
                  order = {'NEW':1,'QED':2})

GC_362 = Coupling(name = 'GC_362',
                  value = '(2*c10b*ee**2*invNscale**2)/sw',
                  order = {'NEW':1,'QED':2})

GC_363 = Coupling(name = 'GC_363',
                  value = '(-6*c10c*ee*complex(0,1)*invNscale**3)/sw',
                  order = {'NEW':1,'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '(6*c10c*ee*complex(0,1)*invNscale**3)/sw',
                  order = {'NEW':1,'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '(6*c10c*ee*invNscale**3)/sw',
                  order = {'NEW':1,'QED':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '(-18*c10c*CKM1x1*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '(-18*c10c*CKM1x2*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(-18*c10c*CKM1x3*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '(-18*c10c*CKM2x1*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '(-18*c10c*CKM2x2*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '(-18*c10c*CKM2x3*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '(-18*c10c*CKM3x1*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '(-18*c10c*CKM3x2*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '(-18*c10c*CKM3x3*ee*complex(0,1)*invNscale**3*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_376 = Coupling(name = 'GC_376',
                  value = '(-18*c10c*cw*ee*complex(0,1)*invNscale**3)/sw',
                  order = {'NEW':1,'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '(18*c10c*cw*ee*complex(0,1)*invNscale**3)/sw',
                  order = {'NEW':1,'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(-6*c10c*ee**2*invNscale**3)/sw',
                  order = {'NEW':1,'QED':2})

GC_379 = Coupling(name = 'GC_379',
                  value = '(6*c10c*ee**2*complex(0,1)*invNscale**3)/sw',
                  order = {'NEW':1,'QED':2})

GC_380 = Coupling(name = 'GC_380',
                  value = '(6*c10c*ee**2*invNscale**3)/sw',
                  order = {'NEW':1,'QED':2})

GC_381 = Coupling(name = 'GC_381',
                  value = '(-24*c10d*ee*complex(0,1)*invNscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '(24*c10d*ee*complex(0,1)*invNscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(24*c10d*ee*invNscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '(-72*c10d*CKM1x1*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '(-72*c10d*CKM1x2*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '(-72*c10d*CKM1x3*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '(-72*c10d*CKM2x1*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '(-72*c10d*CKM2x2*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '(-72*c10d*CKM2x3*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '(-72*c10d*CKM3x1*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(-72*c10d*CKM3x2*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '(-72*c10d*CKM3x3*ee*complex(0,1)*invNscale**4*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '(-72*c10d*cw*ee*complex(0,1)*invNscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '(72*c10d*cw*ee*complex(0,1)*invNscale**4)/sw',
                  order = {'NEW':1,'QED':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '(-24*c10d*ee**2*invNscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_397 = Coupling(name = 'GC_397',
                  value = '(24*c10d*ee**2*complex(0,1)*invNscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_398 = Coupling(name = 'GC_398',
                  value = '(24*c10d*ee**2*invNscale**4)/sw',
                  order = {'NEW':1,'QED':2})

GC_399 = Coupling(name = 'GC_399',
                  value = '-(ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '(ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_401 = Coupling(name = 'GC_401',
                  value = '(-2*c1*ee*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '(c2*ee*complex(0,1)*invMscale**4*sw)/(3.*cw)',
                  order = {'NEW':1,'QED':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*sw)/cw)',
                  order = {'NEW':1,'QED':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '(8*c3a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '(-2*c4a*ee*complex(0,1)*invMscale**8*sw)/(3.*cw)',
                  order = {'NEW':1,'QED':1})

GC_408 = Coupling(name = 'GC_408',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_409 = Coupling(name = 'GC_409',
                  value = '(2*c5a*ee*complex(0,1)*invMscale**8*sw)/(3.*cw)',
                  order = {'NEW':1,'QED':1})

GC_410 = Coupling(name = 'GC_410',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_411 = Coupling(name = 'GC_411',
                  value = '(c10a*ee*complex(0,1)*invNscale*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_412 = Coupling(name = 'GC_412',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_413 = Coupling(name = 'GC_413',
                  value = '(2*c10b*ee*complex(0,1)*invNscale**2*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_414 = Coupling(name = 'GC_414',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_415 = Coupling(name = 'GC_415',
                  value = '(6*c10c*ee*complex(0,1)*invNscale**3*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_416 = Coupling(name = 'GC_416',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_417 = Coupling(name = 'GC_417',
                  value = '(24*c10d*ee*complex(0,1)*invNscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_418 = Coupling(name = 'GC_418',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_419 = Coupling(name = 'GC_419',
                  value = '-(cw*ee)/(2.*sw) - (ee*sw)/(2.*cw)',
                  order = {'QED':1})

GC_420 = Coupling(name = 'GC_420',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_421 = Coupling(name = 'GC_421',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_422 = Coupling(name = 'GC_422',
                  value = '(cw*ee**2*complex(0,1))/sw - (ee**2*complex(0,1)*sw)/cw',
                  order = {'QED':2})

GC_423 = Coupling(name = 'GC_423',
                  value = '(-2*c1*cw*ee*invMscale**4)/sw - (2*c1*ee*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_424 = Coupling(name = 'GC_424',
                  value = '(-2*c1*cw*ee*complex(0,1)*invMscale**4)/sw + (2*c1*ee*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_425 = Coupling(name = 'GC_425',
                  value = '(6*c1*cw*ee*complex(0,1)*invMscale**4)/sw + (6*c1*ee*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_426 = Coupling(name = 'GC_426',
                  value = '-((c2*cw*ee*complex(0,1)*invMscale**4)/sw) - (c2*ee*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_427 = Coupling(name = 'GC_427',
                  value = '(c2*cw*ee*complex(0,1)*invMscale**4)/sw - (c2*ee*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '(c2*cw*ee*invMscale**4)/sw + (c2*ee*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_429 = Coupling(name = 'GC_429',
                  value = '(4*c1*cw*ee**2*complex(0,1)*invMscale**4)/sw - (4*c1*ee**2*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_430 = Coupling(name = 'GC_430',
                  value = '(-2*c2*cw*ee**2*complex(0,1)*invMscale**4)/sw + (2*c2*ee**2*complex(0,1)*invMscale**4*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_431 = Coupling(name = 'GC_431',
                  value = '(8*c3a*cw*ee*complex(0,1)*invMscale**8)/sw - (8*c3a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '(-24*c3a*cw*ee*complex(0,1)*invMscale**8)/sw - (24*c3a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '(8*c3a*cw*ee*invMscale**8)/sw + (8*c3a*ee*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '(-2*c4a*cw*ee*invMscale**8)/sw - (2*c4a*ee*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '(-2*c4a*cw*ee*complex(0,1)*invMscale**8)/sw + (2*c4a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '(2*c4a*cw*ee*complex(0,1)*invMscale**8)/sw + (2*c4a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '(-2*c5a*cw*ee*complex(0,1)*invMscale**8)/sw - (2*c5a*ee*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '(-16*c3a*cw*ee**2*complex(0,1)*invMscale**8)/sw + (16*c3a*ee**2*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_439 = Coupling(name = 'GC_439',
                  value = '(4*c4a*cw*ee**2*complex(0,1)*invMscale**8)/sw - (4*c4a*ee**2*complex(0,1)*invMscale**8*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_440 = Coupling(name = 'GC_440',
                  value = '(c10a*cw*ee*complex(0,1)*invNscale)/sw - (c10a*ee*complex(0,1)*invNscale*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '(-3*c10a*cw*ee*complex(0,1)*invNscale)/sw - (3*c10a*ee*complex(0,1)*invNscale*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '(c10a*cw*ee*invNscale)/sw + (c10a*ee*invNscale*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '(-2*c10a*cw*ee**2*complex(0,1)*invNscale)/sw + (2*c10a*ee**2*complex(0,1)*invNscale*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_444 = Coupling(name = 'GC_444',
                  value = '(2*c10b*cw*ee*complex(0,1)*invNscale**2)/sw - (2*c10b*ee*complex(0,1)*invNscale**2*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_445 = Coupling(name = 'GC_445',
                  value = '(-6*c10b*cw*ee*complex(0,1)*invNscale**2)/sw - (6*c10b*ee*complex(0,1)*invNscale**2*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_446 = Coupling(name = 'GC_446',
                  value = '(2*c10b*cw*ee*invNscale**2)/sw + (2*c10b*ee*invNscale**2*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_447 = Coupling(name = 'GC_447',
                  value = '(-4*c10b*cw*ee**2*complex(0,1)*invNscale**2)/sw + (4*c10b*ee**2*complex(0,1)*invNscale**2*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_448 = Coupling(name = 'GC_448',
                  value = '(6*c10c*cw*ee*complex(0,1)*invNscale**3)/sw - (6*c10c*ee*complex(0,1)*invNscale**3*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '(-18*c10c*cw*ee*complex(0,1)*invNscale**3)/sw - (18*c10c*ee*complex(0,1)*invNscale**3*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '(6*c10c*cw*ee*invNscale**3)/sw + (6*c10c*ee*invNscale**3*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '(-12*c10c*cw*ee**2*complex(0,1)*invNscale**3)/sw + (12*c10c*ee**2*complex(0,1)*invNscale**3*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_452 = Coupling(name = 'GC_452',
                  value = '(24*c10d*cw*ee*complex(0,1)*invNscale**4)/sw - (24*c10d*ee*complex(0,1)*invNscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '(-72*c10d*cw*ee*complex(0,1)*invNscale**4)/sw - (72*c10d*ee*complex(0,1)*invNscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '(24*c10d*cw*ee*invNscale**4)/sw + (24*c10d*ee*invNscale**4*sw)/cw',
                  order = {'NEW':1,'QED':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '(-48*c10d*cw*ee**2*complex(0,1)*invNscale**4)/sw + (48*c10d*ee**2*complex(0,1)*invNscale**4*sw)/cw',
                  order = {'NEW':1,'QED':2})

GC_456 = Coupling(name = 'GC_456',
                  value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_457 = Coupling(name = 'GC_457',
                  value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_458 = Coupling(name = 'GC_458',
                  value = '-4*c1*ee**2*complex(0,1)*invMscale**4 + (2*c1*cw**2*ee**2*complex(0,1)*invMscale**4)/sw**2 + (2*c1*ee**2*complex(0,1)*invMscale**4*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_459 = Coupling(name = 'GC_459',
                  value = '4*c1*ee**2*complex(0,1)*invMscale**4 + (2*c1*cw**2*ee**2*complex(0,1)*invMscale**4)/sw**2 + (2*c1*ee**2*complex(0,1)*invMscale**4*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_460 = Coupling(name = 'GC_460',
                  value = '-2*c2*ee**2*complex(0,1)*invMscale**4 - (c2*cw**2*ee**2*complex(0,1)*invMscale**4)/sw**2 - (c2*ee**2*complex(0,1)*invMscale**4*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_461 = Coupling(name = 'GC_461',
                  value = '2*c2*ee**2*complex(0,1)*invMscale**4 - (c2*cw**2*ee**2*complex(0,1)*invMscale**4)/sw**2 - (c2*ee**2*complex(0,1)*invMscale**4*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_462 = Coupling(name = 'GC_462',
                  value = '-16*c3a*ee**2*complex(0,1)*invMscale**8 - (8*c3a*cw**2*ee**2*complex(0,1)*invMscale**8)/sw**2 - (8*c3a*ee**2*complex(0,1)*invMscale**8*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_463 = Coupling(name = 'GC_463',
                  value = '16*c3a*ee**2*complex(0,1)*invMscale**8 - (8*c3a*cw**2*ee**2*complex(0,1)*invMscale**8)/sw**2 - (8*c3a*ee**2*complex(0,1)*invMscale**8*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_464 = Coupling(name = 'GC_464',
                  value = '-4*c4a*ee**2*complex(0,1)*invMscale**8 + (2*c4a*cw**2*ee**2*complex(0,1)*invMscale**8)/sw**2 + (2*c4a*ee**2*complex(0,1)*invMscale**8*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_465 = Coupling(name = 'GC_465',
                  value = '4*c4a*ee**2*complex(0,1)*invMscale**8 + (2*c4a*cw**2*ee**2*complex(0,1)*invMscale**8)/sw**2 + (2*c4a*ee**2*complex(0,1)*invMscale**8*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_466 = Coupling(name = 'GC_466',
                  value = '-2*c10a*ee**2*complex(0,1)*invNscale - (c10a*cw**2*ee**2*complex(0,1)*invNscale)/sw**2 - (c10a*ee**2*complex(0,1)*invNscale*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_467 = Coupling(name = 'GC_467',
                  value = '2*c10a*ee**2*complex(0,1)*invNscale - (c10a*cw**2*ee**2*complex(0,1)*invNscale)/sw**2 - (c10a*ee**2*complex(0,1)*invNscale*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_468 = Coupling(name = 'GC_468',
                  value = '-4*c10b*ee**2*complex(0,1)*invNscale**2 - (2*c10b*cw**2*ee**2*complex(0,1)*invNscale**2)/sw**2 - (2*c10b*ee**2*complex(0,1)*invNscale**2*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_469 = Coupling(name = 'GC_469',
                  value = '4*c10b*ee**2*complex(0,1)*invNscale**2 - (2*c10b*cw**2*ee**2*complex(0,1)*invNscale**2)/sw**2 - (2*c10b*ee**2*complex(0,1)*invNscale**2*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_470 = Coupling(name = 'GC_470',
                  value = '-12*c10c*ee**2*complex(0,1)*invNscale**3 - (6*c10c*cw**2*ee**2*complex(0,1)*invNscale**3)/sw**2 - (6*c10c*ee**2*complex(0,1)*invNscale**3*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_471 = Coupling(name = 'GC_471',
                  value = '12*c10c*ee**2*complex(0,1)*invNscale**3 - (6*c10c*cw**2*ee**2*complex(0,1)*invNscale**3)/sw**2 - (6*c10c*ee**2*complex(0,1)*invNscale**3*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_472 = Coupling(name = 'GC_472',
                  value = '-48*c10d*ee**2*complex(0,1)*invNscale**4 - (24*c10d*cw**2*ee**2*complex(0,1)*invNscale**4)/sw**2 - (24*c10d*ee**2*complex(0,1)*invNscale**4*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_473 = Coupling(name = 'GC_473',
                  value = '48*c10d*ee**2*complex(0,1)*invNscale**4 - (24*c10d*cw**2*ee**2*complex(0,1)*invNscale**4)/sw**2 - (24*c10d*ee**2*complex(0,1)*invNscale**4*sw**2)/cw**2',
                  order = {'NEW':1,'QED':2})

GC_474 = Coupling(name = 'GC_474',
                  value = '-(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_476 = Coupling(name = 'GC_476',
                  value = '(-2*c1*ee**2*invMscale**4*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_477 = Coupling(name = 'GC_477',
                  value = '(2*c1*ee**2*invMscale**4*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_478 = Coupling(name = 'GC_478',
                  value = '-((c2*ee**2*invMscale**4*vev)/cw)',
                  order = {'NEW':1,'QED':1})

GC_479 = Coupling(name = 'GC_479',
                  value = '(c2*ee**2*invMscale**4*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_480 = Coupling(name = 'GC_480',
                  value = '(-8*c3a*ee**2*invMscale**8*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_481 = Coupling(name = 'GC_481',
                  value = '(8*c3a*ee**2*invMscale**8*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_482 = Coupling(name = 'GC_482',
                  value = '(-2*c4a*ee**2*invMscale**8*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_483 = Coupling(name = 'GC_483',
                  value = '(2*c4a*ee**2*invMscale**8*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_484 = Coupling(name = 'GC_484',
                  value = '-((c10a*ee**2*invNscale*vev)/cw)',
                  order = {'NEW':1,'QED':1})

GC_485 = Coupling(name = 'GC_485',
                  value = '(c10a*ee**2*invNscale*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_486 = Coupling(name = 'GC_486',
                  value = '(-2*c10b*ee**2*invNscale**2*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_487 = Coupling(name = 'GC_487',
                  value = '(2*c10b*ee**2*invNscale**2*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_488 = Coupling(name = 'GC_488',
                  value = '(-6*c10c*ee**2*invNscale**3*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_489 = Coupling(name = 'GC_489',
                  value = '(6*c10c*ee**2*invNscale**3*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_490 = Coupling(name = 'GC_490',
                  value = '(-24*c10d*ee**2*invNscale**4*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_491 = Coupling(name = 'GC_491',
                  value = '(24*c10d*ee**2*invNscale**4*vev)/cw',
                  order = {'NEW':1,'QED':1})

GC_492 = Coupling(name = 'GC_492',
                  value = '-2*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_493 = Coupling(name = 'GC_493',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_494 = Coupling(name = 'GC_494',
                  value = '8*c10a*complex(0,1)*invNscale*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_495 = Coupling(name = 'GC_495',
                  value = '24*c10a*complex(0,1)*invNscale*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_496 = Coupling(name = 'GC_496',
                  value = '16*c10b*complex(0,1)*invNscale**2*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_497 = Coupling(name = 'GC_497',
                  value = '48*c10b*complex(0,1)*invNscale**2*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_498 = Coupling(name = 'GC_498',
                  value = '48*c10c*complex(0,1)*invNscale**3*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_499 = Coupling(name = 'GC_499',
                  value = '144*c10c*complex(0,1)*invNscale**3*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_500 = Coupling(name = 'GC_500',
                  value = '192*c10d*complex(0,1)*invNscale**4*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_501 = Coupling(name = 'GC_501',
                  value = '576*c10d*complex(0,1)*invNscale**4*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_502 = Coupling(name = 'GC_502',
                  value = '-(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_503 = Coupling(name = 'GC_503',
                  value = '-(ee**2*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_504 = Coupling(name = 'GC_504',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_505 = Coupling(name = 'GC_505',
                  value = '(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_506 = Coupling(name = 'GC_506',
                  value = '(2*c1*ee**2*complex(0,1)*invMscale**4*vev)/sw**2',
                  order = {'NEW':1,'QED':1})

GC_507 = Coupling(name = 'GC_507',
                  value = '-((c2*ee**2*complex(0,1)*invMscale**4*vev)/sw**2)',
                  order = {'NEW':1,'QED':1})

GC_508 = Coupling(name = 'GC_508',
                  value = '(-8*c3a*ee**2*complex(0,1)*invMscale**8*vev)/sw**2',
                  order = {'NEW':1,'QED':1})

GC_509 = Coupling(name = 'GC_509',
                  value = '(2*c4a*ee**2*complex(0,1)*invMscale**8*vev)/sw**2',
                  order = {'NEW':1,'QED':1})

GC_510 = Coupling(name = 'GC_510',
                  value = '-((c10a*ee**2*complex(0,1)*invNscale*vev)/sw**2)',
                  order = {'NEW':1,'QED':1})

GC_511 = Coupling(name = 'GC_511',
                  value = '(-2*c10b*ee**2*complex(0,1)*invNscale**2*vev)/sw**2',
                  order = {'NEW':1,'QED':1})

GC_512 = Coupling(name = 'GC_512',
                  value = '(-6*c10c*ee**2*complex(0,1)*invNscale**3*vev)/sw**2',
                  order = {'NEW':1,'QED':1})

GC_513 = Coupling(name = 'GC_513',
                  value = '(-24*c10d*ee**2*complex(0,1)*invNscale**4*vev)/sw**2',
                  order = {'NEW':1,'QED':1})

GC_514 = Coupling(name = 'GC_514',
                  value = '-(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_516 = Coupling(name = 'GC_516',
                  value = '(-2*c1*ee*invMscale**4*vev)/sw',
                  order = {'NEW':1})

GC_517 = Coupling(name = 'GC_517',
                  value = '(c2*ee*invMscale**4*vev)/sw',
                  order = {'NEW':1})

GC_518 = Coupling(name = 'GC_518',
                  value = '(-2*c1*ee**2*invMscale**4*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_519 = Coupling(name = 'GC_519',
                  value = '(2*c1*ee**2*invMscale**4*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_520 = Coupling(name = 'GC_520',
                  value = '-((c2*ee**2*invMscale**4*vev)/sw)',
                  order = {'NEW':1,'QED':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '(c2*ee**2*invMscale**4*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_522 = Coupling(name = 'GC_522',
                  value = '(8*c3a*ee*invMscale**8*vev)/sw',
                  order = {'NEW':1})

GC_523 = Coupling(name = 'GC_523',
                  value = '(-2*c4a*ee*invMscale**8*vev)/sw',
                  order = {'NEW':1})

GC_524 = Coupling(name = 'GC_524',
                  value = '(-8*c3a*ee**2*invMscale**8*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_525 = Coupling(name = 'GC_525',
                  value = '(8*c3a*ee**2*invMscale**8*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_526 = Coupling(name = 'GC_526',
                  value = '(-2*c4a*ee**2*invMscale**8*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_527 = Coupling(name = 'GC_527',
                  value = '(2*c4a*ee**2*invMscale**8*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '(c10a*ee*invNscale*vev)/sw',
                  order = {'NEW':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '-((c10a*ee**2*invNscale*vev)/sw)',
                  order = {'NEW':1,'QED':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '(c10a*ee**2*invNscale*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_531 = Coupling(name = 'GC_531',
                  value = '(2*c10b*ee*invNscale**2*vev)/sw',
                  order = {'NEW':1})

GC_532 = Coupling(name = 'GC_532',
                  value = '(-2*c10b*ee**2*invNscale**2*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_533 = Coupling(name = 'GC_533',
                  value = '(2*c10b*ee**2*invNscale**2*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_534 = Coupling(name = 'GC_534',
                  value = '(6*c10c*ee*invNscale**3*vev)/sw',
                  order = {'NEW':1})

GC_535 = Coupling(name = 'GC_535',
                  value = '(-6*c10c*ee**2*invNscale**3*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_536 = Coupling(name = 'GC_536',
                  value = '(6*c10c*ee**2*invNscale**3*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_537 = Coupling(name = 'GC_537',
                  value = '(24*c10d*ee*invNscale**4*vev)/sw',
                  order = {'NEW':1})

GC_538 = Coupling(name = 'GC_538',
                  value = '(-24*c10d*ee**2*invNscale**4*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_539 = Coupling(name = 'GC_539',
                  value = '(24*c10d*ee**2*invNscale**4*vev)/sw',
                  order = {'NEW':1,'QED':1})

GC_540 = Coupling(name = 'GC_540',
                  value = '8*c10a*complex(0,1)*invNscale*lam*vev**2',
                  order = {'NEW':1})

GC_541 = Coupling(name = 'GC_541',
                  value = '16*c10b*complex(0,1)*invNscale**2*lam*vev**2',
                  order = {'NEW':1})

GC_542 = Coupling(name = 'GC_542',
                  value = '48*c10c*complex(0,1)*invNscale**3*lam*vev**2',
                  order = {'NEW':1})

GC_543 = Coupling(name = 'GC_543',
                  value = '192*c10d*complex(0,1)*invNscale**4*lam*vev**2',
                  order = {'NEW':1})

GC_544 = Coupling(name = 'GC_544',
                  value = '-6*c10c*complex(0,1)*invNscale**3*lam*vev**4',
                  order = {'NEW':1,'QED':-2})

GC_545 = Coupling(name = 'GC_545',
                  value = '-24*c10d*complex(0,1)*invNscale**4*lam*vev**4',
                  order = {'NEW':1,'QED':-2})

GC_546 = Coupling(name = 'GC_546',
                  value = '-16*c1*complex(0,1)*invMscale**4*lam*vev - 4*c2*complex(0,1)*invMscale**4*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_547 = Coupling(name = 'GC_547',
                  value = '-48*c1*complex(0,1)*invMscale**4*lam*vev - 12*c2*complex(0,1)*invMscale**4*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_548 = Coupling(name = 'GC_548',
                  value = '64*c3a*complex(0,1)*invMscale**8*lam*vev + 16*c4a*complex(0,1)*invMscale**8*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_549 = Coupling(name = 'GC_549',
                  value = '192*c3a*complex(0,1)*invMscale**8*lam*vev + 48*c4a*complex(0,1)*invMscale**8*lam*vev',
                  order = {'NEW':1,'QED':1})

GC_550 = Coupling(name = 'GC_550',
                  value = '-(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_551 = Coupling(name = 'GC_551',
                  value = '(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '-(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_553 = Coupling(name = 'GC_553',
                  value = '(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_554 = Coupling(name = 'GC_554',
                  value = '(-2*c1*cw*ee*invMscale**4*vev)/sw - (2*c1*ee*invMscale**4*sw*vev)/cw',
                  order = {'NEW':1})

GC_555 = Coupling(name = 'GC_555',
                  value = '(c2*cw*ee*invMscale**4*vev)/sw + (c2*ee*invMscale**4*sw*vev)/cw',
                  order = {'NEW':1})

GC_556 = Coupling(name = 'GC_556',
                  value = '(8*c3a*cw*ee*invMscale**8*vev)/sw + (8*c3a*ee*invMscale**8*sw*vev)/cw',
                  order = {'NEW':1})

GC_557 = Coupling(name = 'GC_557',
                  value = '(-2*c4a*cw*ee*invMscale**8*vev)/sw - (2*c4a*ee*invMscale**8*sw*vev)/cw',
                  order = {'NEW':1})

GC_558 = Coupling(name = 'GC_558',
                  value = '(c10a*cw*ee*invNscale*vev)/sw + (c10a*ee*invNscale*sw*vev)/cw',
                  order = {'NEW':1})

GC_559 = Coupling(name = 'GC_559',
                  value = '(2*c10b*cw*ee*invNscale**2*vev)/sw + (2*c10b*ee*invNscale**2*sw*vev)/cw',
                  order = {'NEW':1})

GC_560 = Coupling(name = 'GC_560',
                  value = '(6*c10c*cw*ee*invNscale**3*vev)/sw + (6*c10c*ee*invNscale**3*sw*vev)/cw',
                  order = {'NEW':1})

GC_561 = Coupling(name = 'GC_561',
                  value = '(24*c10d*cw*ee*invNscale**4*vev)/sw + (24*c10d*ee*invNscale**4*sw*vev)/cw',
                  order = {'NEW':1})

GC_562 = Coupling(name = 'GC_562',
                  value = '-(ee**2*complex(0,1)*vev)/2. - (cw**2*ee**2*complex(0,1)*vev)/(4.*sw**2) - (ee**2*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'QED':1})

GC_563 = Coupling(name = 'GC_563',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_564 = Coupling(name = 'GC_564',
                  value = '4*c1*ee**2*complex(0,1)*invMscale**4*vev + (2*c1*cw**2*ee**2*complex(0,1)*invMscale**4*vev)/sw**2 + (2*c1*ee**2*complex(0,1)*invMscale**4*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_565 = Coupling(name = 'GC_565',
                  value = '-2*c2*ee**2*complex(0,1)*invMscale**4*vev - (c2*cw**2*ee**2*complex(0,1)*invMscale**4*vev)/sw**2 - (c2*ee**2*complex(0,1)*invMscale**4*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '-16*c3a*ee**2*complex(0,1)*invMscale**8*vev - (8*c3a*cw**2*ee**2*complex(0,1)*invMscale**8*vev)/sw**2 - (8*c3a*ee**2*complex(0,1)*invMscale**8*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '4*c4a*ee**2*complex(0,1)*invMscale**8*vev + (2*c4a*cw**2*ee**2*complex(0,1)*invMscale**8*vev)/sw**2 + (2*c4a*ee**2*complex(0,1)*invMscale**8*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_568 = Coupling(name = 'GC_568',
                  value = '-2*c10a*ee**2*complex(0,1)*invNscale*vev - (c10a*cw**2*ee**2*complex(0,1)*invNscale*vev)/sw**2 - (c10a*ee**2*complex(0,1)*invNscale*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '-4*c10b*ee**2*complex(0,1)*invNscale**2*vev - (2*c10b*cw**2*ee**2*complex(0,1)*invNscale**2*vev)/sw**2 - (2*c10b*ee**2*complex(0,1)*invNscale**2*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_570 = Coupling(name = 'GC_570',
                  value = '-12*c10c*ee**2*complex(0,1)*invNscale**3*vev - (6*c10c*cw**2*ee**2*complex(0,1)*invNscale**3*vev)/sw**2 - (6*c10c*ee**2*complex(0,1)*invNscale**3*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_571 = Coupling(name = 'GC_571',
                  value = '-48*c10d*ee**2*complex(0,1)*invNscale**4*vev - (24*c10d*cw**2*ee**2*complex(0,1)*invNscale**4*vev)/sw**2 - (24*c10d*ee**2*complex(0,1)*invNscale**4*sw**2*vev)/cw**2',
                  order = {'NEW':1,'QED':1})

GC_572 = Coupling(name = 'GC_572',
                  value = '-16*c1*complex(0,1)*invMscale**4*lam*vev**2 - 4*c2*complex(0,1)*invMscale**4*lam*vev**2',
                  order = {'NEW':1})

GC_573 = Coupling(name = 'GC_573',
                  value = '64*c3a*complex(0,1)*invMscale**8*lam*vev**2 + 16*c4a*complex(0,1)*invMscale**8*lam*vev**2',
                  order = {'NEW':1})

GC_574 = Coupling(name = 'GC_574',
                  value = '8*c1*complex(0,1)*invMscale**4*MW**2 + (c1*ee**2*complex(0,1)*invMscale**4*vev**2)/sw**2',
                  order = {'NEW':1})

GC_575 = Coupling(name = 'GC_575',
                  value = '-4*c2*complex(0,1)*invMscale**4*MW**2 - (c2*ee**2*complex(0,1)*invMscale**4*vev**2)/(2.*sw**2)',
                  order = {'NEW':1})

GC_576 = Coupling(name = 'GC_576',
                  value = '-32*c3a*complex(0,1)*invMscale**8*MW**2 - (4*c3a*ee**2*complex(0,1)*invMscale**8*vev**2)/sw**2',
                  order = {'NEW':1})

GC_577 = Coupling(name = 'GC_577',
                  value = '8*c4a*complex(0,1)*invMscale**8*MW**2 + (c4a*ee**2*complex(0,1)*invMscale**8*vev**2)/sw**2',
                  order = {'NEW':1})

GC_578 = Coupling(name = 'GC_578',
                  value = '-4*c10a*complex(0,1)*invNscale*MW**2 - (c10a*ee**2*complex(0,1)*invNscale*vev**2)/(2.*sw**2)',
                  order = {'NEW':1})

GC_579 = Coupling(name = 'GC_579',
                  value = '-8*c10b*complex(0,1)*invNscale**2*MW**2 - (c10b*ee**2*complex(0,1)*invNscale**2*vev**2)/sw**2',
                  order = {'NEW':1})

GC_580 = Coupling(name = 'GC_580',
                  value = '-24*c10c*complex(0,1)*invNscale**3*MW**2 - (3*c10c*ee**2*complex(0,1)*invNscale**3*vev**2)/sw**2',
                  order = {'NEW':1})

GC_581 = Coupling(name = 'GC_581',
                  value = '-96*c10d*complex(0,1)*invNscale**4*MW**2 - (12*c10d*ee**2*complex(0,1)*invNscale**4*vev**2)/sw**2',
                  order = {'NEW':1})

GC_582 = Coupling(name = 'GC_582',
                  value = '8*c1*complex(0,1)*invMscale**4*MZ**2 + 2*c1*ee**2*complex(0,1)*invMscale**4*vev**2 + (c1*cw**2*ee**2*complex(0,1)*invMscale**4*vev**2)/sw**2 + (c1*ee**2*complex(0,1)*invMscale**4*sw**2*vev**2)/cw**2',
                  order = {'NEW':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '-4*c2*complex(0,1)*invMscale**4*MZ**2 - c2*ee**2*complex(0,1)*invMscale**4*vev**2 - (c2*cw**2*ee**2*complex(0,1)*invMscale**4*vev**2)/(2.*sw**2) - (c2*ee**2*complex(0,1)*invMscale**4*sw**2*vev**2)/(2.*cw**2)',
                  order = {'NEW':1})

GC_584 = Coupling(name = 'GC_584',
                  value = '-32*c3a*complex(0,1)*invMscale**8*MZ**2 - 8*c3a*ee**2*complex(0,1)*invMscale**8*vev**2 - (4*c3a*cw**2*ee**2*complex(0,1)*invMscale**8*vev**2)/sw**2 - (4*c3a*ee**2*complex(0,1)*invMscale**8*sw**2*vev**2)/cw**2',
                  order = {'NEW':1})

GC_585 = Coupling(name = 'GC_585',
                  value = '8*c4a*complex(0,1)*invMscale**8*MZ**2 + 2*c4a*ee**2*complex(0,1)*invMscale**8*vev**2 + (c4a*cw**2*ee**2*complex(0,1)*invMscale**8*vev**2)/sw**2 + (c4a*ee**2*complex(0,1)*invMscale**8*sw**2*vev**2)/cw**2',
                  order = {'NEW':1})

GC_586 = Coupling(name = 'GC_586',
                  value = '-4*c10a*complex(0,1)*invNscale*MZ**2 - c10a*ee**2*complex(0,1)*invNscale*vev**2 - (c10a*cw**2*ee**2*complex(0,1)*invNscale*vev**2)/(2.*sw**2) - (c10a*ee**2*complex(0,1)*invNscale*sw**2*vev**2)/(2.*cw**2)',
                  order = {'NEW':1})

GC_587 = Coupling(name = 'GC_587',
                  value = '-8*c10b*complex(0,1)*invNscale**2*MZ**2 - 2*c10b*ee**2*complex(0,1)*invNscale**2*vev**2 - (c10b*cw**2*ee**2*complex(0,1)*invNscale**2*vev**2)/sw**2 - (c10b*ee**2*complex(0,1)*invNscale**2*sw**2*vev**2)/cw**2',
                  order = {'NEW':1})

GC_588 = Coupling(name = 'GC_588',
                  value = '-24*c10c*complex(0,1)*invNscale**3*MZ**2 - 6*c10c*ee**2*complex(0,1)*invNscale**3*vev**2 - (3*c10c*cw**2*ee**2*complex(0,1)*invNscale**3*vev**2)/sw**2 - (3*c10c*ee**2*complex(0,1)*invNscale**3*sw**2*vev**2)/cw**2',
                  order = {'NEW':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '-96*c10d*complex(0,1)*invNscale**4*MZ**2 - 24*c10d*ee**2*complex(0,1)*invNscale**4*vev**2 - (12*c10d*cw**2*ee**2*complex(0,1)*invNscale**4*vev**2)/sw**2 - (12*c10d*ee**2*complex(0,1)*invNscale**4*sw**2*vev**2)/cw**2',
                  order = {'NEW':1})

GC_590 = Coupling(name = 'GC_590',
                  value = '-8*c3a*complex(0,1)*invMscale**8*lam*vev**4 - 2*c4a*complex(0,1)*invMscale**8*lam*vev**4',
                  order = {'NEW':1,'QED':-2})

GC_591 = Coupling(name = 'GC_591',
                  value = '-(yb/cmath.sqrt(2))',
                  order = {'QED':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_593 = Coupling(name = 'GC_593',
                  value = '-((complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_594 = Coupling(name = 'GC_594',
                  value = 'yc/cmath.sqrt(2)',
                  order = {'QED':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '-(ydo/cmath.sqrt(2))',
                  order = {'QED':1})

GC_596 = Coupling(name = 'GC_596',
                  value = '-((complex(0,1)*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_597 = Coupling(name = 'GC_597',
                  value = '-ye',
                  order = {'QED':1})

GC_598 = Coupling(name = 'GC_598',
                  value = 'ye',
                  order = {'QED':1})

GC_599 = Coupling(name = 'GC_599',
                  value = '-(ye/cmath.sqrt(2))',
                  order = {'QED':1})

GC_600 = Coupling(name = 'GC_600',
                  value = '-((complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_601 = Coupling(name = 'GC_601',
                  value = '-ym',
                  order = {'QED':1})

GC_602 = Coupling(name = 'GC_602',
                  value = 'ym',
                  order = {'QED':1})

GC_603 = Coupling(name = 'GC_603',
                  value = '-(ym/cmath.sqrt(2))',
                  order = {'QED':1})

GC_604 = Coupling(name = 'GC_604',
                  value = '-((complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_605 = Coupling(name = 'GC_605',
                  value = '-(ys/cmath.sqrt(2))',
                  order = {'QED':1})

GC_606 = Coupling(name = 'GC_606',
                  value = '-((complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_607 = Coupling(name = 'GC_607',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_608 = Coupling(name = 'GC_608',
                  value = 'yt/cmath.sqrt(2)',
                  order = {'QED':1})

GC_609 = Coupling(name = 'GC_609',
                  value = '-ytau',
                  order = {'QED':1})

GC_610 = Coupling(name = 'GC_610',
                  value = 'ytau',
                  order = {'QED':1})

GC_611 = Coupling(name = 'GC_611',
                  value = '-(ytau/cmath.sqrt(2))',
                  order = {'QED':1})

GC_612 = Coupling(name = 'GC_612',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_613 = Coupling(name = 'GC_613',
                  value = '-((complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_614 = Coupling(name = 'GC_614',
                  value = 'yup/cmath.sqrt(2)',
                  order = {'QED':1})

GC_615 = Coupling(name = 'GC_615',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_616 = Coupling(name = 'GC_616',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_617 = Coupling(name = 'GC_617',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_618 = Coupling(name = 'GC_618',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_619 = Coupling(name = 'GC_619',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_620 = Coupling(name = 'GC_620',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_621 = Coupling(name = 'GC_621',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_622 = Coupling(name = 'GC_622',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_623 = Coupling(name = 'GC_623',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_624 = Coupling(name = 'GC_624',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM1x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_625 = Coupling(name = 'GC_625',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_626 = Coupling(name = 'GC_626',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_627 = Coupling(name = 'GC_627',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_628 = Coupling(name = 'GC_628',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_629 = Coupling(name = 'GC_629',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_630 = Coupling(name = 'GC_630',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_631 = Coupling(name = 'GC_631',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_632 = Coupling(name = 'GC_632',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_633 = Coupling(name = 'GC_633',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_634 = Coupling(name = 'GC_634',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM1x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_635 = Coupling(name = 'GC_635',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_636 = Coupling(name = 'GC_636',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_637 = Coupling(name = 'GC_637',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_638 = Coupling(name = 'GC_638',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_639 = Coupling(name = 'GC_639',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_640 = Coupling(name = 'GC_640',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_641 = Coupling(name = 'GC_641',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_642 = Coupling(name = 'GC_642',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_643 = Coupling(name = 'GC_643',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_644 = Coupling(name = 'GC_644',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM1x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_645 = Coupling(name = 'GC_645',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_646 = Coupling(name = 'GC_646',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_647 = Coupling(name = 'GC_647',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_648 = Coupling(name = 'GC_648',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_649 = Coupling(name = 'GC_649',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_650 = Coupling(name = 'GC_650',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_651 = Coupling(name = 'GC_651',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_652 = Coupling(name = 'GC_652',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_653 = Coupling(name = 'GC_653',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_654 = Coupling(name = 'GC_654',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM2x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_655 = Coupling(name = 'GC_655',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_656 = Coupling(name = 'GC_656',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_657 = Coupling(name = 'GC_657',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_658 = Coupling(name = 'GC_658',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_659 = Coupling(name = 'GC_659',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_660 = Coupling(name = 'GC_660',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_661 = Coupling(name = 'GC_661',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_662 = Coupling(name = 'GC_662',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_663 = Coupling(name = 'GC_663',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_664 = Coupling(name = 'GC_664',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM2x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_665 = Coupling(name = 'GC_665',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_666 = Coupling(name = 'GC_666',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_667 = Coupling(name = 'GC_667',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_668 = Coupling(name = 'GC_668',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_669 = Coupling(name = 'GC_669',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_670 = Coupling(name = 'GC_670',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_671 = Coupling(name = 'GC_671',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_672 = Coupling(name = 'GC_672',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_673 = Coupling(name = 'GC_673',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_674 = Coupling(name = 'GC_674',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM2x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_675 = Coupling(name = 'GC_675',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_676 = Coupling(name = 'GC_676',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_677 = Coupling(name = 'GC_677',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_678 = Coupling(name = 'GC_678',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_679 = Coupling(name = 'GC_679',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_680 = Coupling(name = 'GC_680',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_681 = Coupling(name = 'GC_681',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_682 = Coupling(name = 'GC_682',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_683 = Coupling(name = 'GC_683',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_684 = Coupling(name = 'GC_684',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM3x1)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_685 = Coupling(name = 'GC_685',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_686 = Coupling(name = 'GC_686',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_687 = Coupling(name = 'GC_687',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_688 = Coupling(name = 'GC_688',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_689 = Coupling(name = 'GC_689',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_690 = Coupling(name = 'GC_690',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_691 = Coupling(name = 'GC_691',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_692 = Coupling(name = 'GC_692',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_693 = Coupling(name = 'GC_693',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_694 = Coupling(name = 'GC_694',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM3x2)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_695 = Coupling(name = 'GC_695',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_696 = Coupling(name = 'GC_696',
                  value = '(6*c1*ee*complex(0,1)*invMscale**4*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_697 = Coupling(name = 'GC_697',
                  value = '-((c2*ee*complex(0,1)*invMscale**4*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw)',
                  order = {'NEW':1,'QED':1})

GC_698 = Coupling(name = 'GC_698',
                  value = '(-24*c3a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_699 = Coupling(name = 'GC_699',
                  value = '(2*c4a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_700 = Coupling(name = 'GC_700',
                  value = '(-2*c5a*ee*complex(0,1)*invMscale**8*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_701 = Coupling(name = 'GC_701',
                  value = '(-3*c10a*ee*complex(0,1)*invNscale*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_702 = Coupling(name = 'GC_702',
                  value = '(-6*c10b*ee*complex(0,1)*invNscale**2*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_703 = Coupling(name = 'GC_703',
                  value = '(-18*c10c*ee*complex(0,1)*invNscale**3*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

GC_704 = Coupling(name = 'GC_704',
                  value = '(-72*c10d*ee*complex(0,1)*invNscale**4*complexconjugate(CKM3x3)*cmath.sqrt(2))/sw',
                  order = {'NEW':1,'QED':1})

