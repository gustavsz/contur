# import cPickle as pickle
import os
import pickle
from collections import defaultdict

import contur
import numpy as np
from contur.factories.yoda_factories import YodaFactory


class Depot(object):
    """Class defining the engine for the analysis, this should be the starting point of any module interaction, processes YODA files and builds plottable map files

    :Public Members:
        * **inbox** (:class:`contur.paramYodaPoint`) *list* --
          The central member of the Depot class, the inbox for all processed objects.
          Example methods to append to this :func:`contur.conturDepot.addParamPoint` and :func:`contur.conturDepot.addCustomParamPoint`
    :Keyword Arguments:
        * *outdir* (``string``) --
          path to output plot objects (*Default=* ``plots``)
        * *noStack* (``bool``) --
          Mark true to not stack the signal on background in plotting (*cosmetic*) (*Default=* ``False``)

    """

    def __init__(self, outputDir="plots", noStack=False):

        self.inbox = []
        self._bookmapAxis = defaultdict(list)
        self.meshGrid = None
        self.gridPoints = None

        # map axis and plot axis are two different objects due to idiosyncracies in pcolormesh
        # mapAxis is the true sampled points
        # plotAxis is the offset values for plotting
        # _bookmapAxis is just for bookkeeping
        self._bookmapAxis = defaultdict(list)
        self._mapAxis = {}
        self._plotAxis = {}

        self._OutputDir = outputDir
        self._NoStack = noStack

    def build_axes(self):
        """Function to build the axis dictionaries used for plotting, parameter space points are otherwise stored unordered

        :Built variables:
            * **mapAxis** (``dict``)
            * **plotAxis** (``dict``)

        """
        for i in self.inbox:
            for k, v in i.paramPoint.items():
                self._bookmapAxis[k].append(v)

        # Pcolormesh (which we use for visualisation) centres grid in bottom left corner of a cell, we need to shift the axis
        # this is a pain but we will truncate at the original max/min anyway so a guess is fine
        # first build the true axes
        for k, v in self._bookmapAxis.items():
            if k != "slha_file":
                self._mapAxis[k] = np.unique(np.array(v, dtype=float))
                self._plotAxis[k] = (
                                        self._mapAxis[k][1:] + self._mapAxis[k][:-1]) / 2
                # now the ugly offset
                if self._plotAxis[k].any():
                    try:
                        self._plotAxis[k] = np.insert(self._plotAxis[k], 0, self._plotAxis[k][0] - self._plotAxis[k][
                            1])  # -self.mapAxis[k][0])
                        self._plotAxis[k] = np.append(self._plotAxis[k], self._mapAxis[k][-1] + (
                            self._mapAxis[k][-1] - self._plotAxis[k][-1]))
                    except:
                        self._plotAxis[k] = self._mapAxis[k]
                else:
                    self._plotAxis[k] = self._mapAxis[k]

    def write(self, outDir):
        """Function to write out a "map" file containing the full pickle of this conturDepot instance

        :param outDir:
            String of filesystem location to write out the pickle of this instance to
        :type outDir: ``string``

        """
        contur.util.mkoutdir(outDir)
        path_out = os.path.join(outDir, 'contur.map')

        contur.config.contur_log.info("Writing output map to : " + path_out)

        with open(path_out, 'wb') as f:
            pickle.dump(self, f, protocol=2)

    def add_custom_point(self, logfile, param_dict):
        """Function to add a custom file, typically read as a string, to the """
        self.inbox.append(ParamYodaPoint(
            yodaFactory=logfile, paramPoint=param_dict))

    def add_point(self, yodafile, param_dict):
        """Add yoda file and the corresponding parameter point into the depot"""
        yFact = YodaFactory(yodaFilePath=yodafile,outdir=self._OutputDir, noStack=self._NoStack)
        yFact.sortBuckets()
        yFact.buildFinal()
        contur.config.contur_log.info(
            "Added yodafile with reported exclusion of: " + str(yFact.conturPoint.CLs))
        self.inbox.append(ParamYodaPoint(
            yodaFactory=yFact, paramPoint=param_dict))

    def resort_points(self):
        """Function to trigger rerunning of the sorting algorithm on all items in the inbox, typically if this list has been affected by a merge by a call to :func:`contur.conturDepot.mergeMaps`
        """
        # convenience function to re-run the sorting algorithm on all points in the depot incase of modification of underlying tests
        for p in self.inbox:
            # p.yodaFactory._testMethod=self._testMethod
            p.yodaFactory._sortSortedBuckets()
            # del p.paramHolder.conturPoints

    def merge(self, conturDepot):
        """Function to merge one conturDepot instance with another, note matching of parameter point for each item in the target instance is required or the entry will be skipped

        :param conturDepot:
            Additional instance to conturDepot to merge with this one
        :type conturDepot: :class:`contur.conturDepot`

        """
        for point in conturDepot.inbox:
            for p in self.inbox:
                same = True
                for key, value in p.paramPoint.items():
                    try:
                        if point.paramPoint[key] != value and not key.startswith("AUX:"):
                            same = False
                    except:
                        contur.config.contur_log.warning(
                            "Not merging. Key not found:" + key)
                        same = False
                if same:
                    p.yodaFactory.sortedBuckets.extend(
                        point.yodaFactory.sortedBuckets)

                    #[p.yodaFactory.sortedBuckets.extend(point.yodaFactory.sortedBuckets) for p in self.conturDepotInbox if p.paramPoint == point.paramPoint]

    def _build_frame(self):
        """:return pandas.DataFrame of the inbox points"""
        try:
            import pandas as pd
        except:
            contur.config.contur_log.error("Pandas module not available")

        #Use the first point in the inbox to make a frame
        try:
            points=pd.DataFrame([x.paramPoint for x in self.inbox])
            cls=pd.DataFrame([x.yodaFactory.conturPoint.CLs for x in self.inbox], columns=["CLs"])
            return points.merge(cls,left_index=True,right_index=True)
        except:
            contur.config.contur_log.error("Inbox is empty, add parameter points to depot")



    @property
    def frame(self):
        return self._build_frame()

    @property
    def map_axis(self):
        """Dictionary of the sampled values in each axis

        :return: Dictionary -- **key** Parameter name (``string``), **value** (:class:`numpy.array`)
        """
        return self._mapAxis

    @map_axis.setter
    def map_axis(self, value):
        self._mapAxis = value

    @property
    def plot_axis(self):
        """Dictionary of the offset midpoints in each axis, for colormesh purposes

        :return: Dictionary -- **key** Parameter name (``string``), **value** (:class:`numpy.array`)
        """
        return self._plotAxis

    @plot_axis.setter
    def plot_axis(self, value):
        self._plotAxis = value

    def __repr__(self):
        return "%s with %s added points" % (self.__class__.__name__, len(self.inbox))


class ParamYodaPoint(object):
    """Book keeping class to define a data structure relating a :class:`contur.conturDepot.yodaFactory` and it's point in parameter space

    :param paramPoint:
        **key** ``string`` param name : **value** ``float``
    :type paramPoint: ``dict``
    :param yodaFactory:
        yodaFactory object, conturs YODA file reader
    :type yodaFactory: :class:`contur.conturDepot.yodaFactory`

    """

    def __init__(self, paramPoint, yodaFactory):
        self.paramPoint = paramPoint
        self.yodaFactory = yodaFactory

    def __repr__(self):
        return repr(self.paramPoint)


