#!/usr/bin/env python

import os
import numpy as np
import yoda
import rivet
import contur


#set the version here globally
global version
version = " Beta pre-release"

def writeBanner():
    """Write Ascii banner"""
    contur.config.contur_log.info("Running Contur version "+contur.config.version)
    contur.config.contur_log.info("See https://hepcedar.gitlab.io/contur-webpage/")

def mkoutdir(outdir):
    "Function to make output directories"
    if not os.path.exists(outdir):
        try:
            os.makedirs(outdir)
        except:
            msg = "Can't make output directory '%s'" % outdir
            raise Exception(msg)
    if not os.access(outdir, os.W_OK):
        msg = "Can't write to output directory '%s'" % outdir
        raise Exception(msg)

def writeOutput(output, h):
    mkoutdir("ANALYSIS")
    f = open("./ANALYSIS/"+h, 'w')
    for item in output:
        f.write(str(item) + "\n")
    f.close()


def getHistos(filelist):
    """Loop over all input files. Only use the first occurrence of any REF-histogram
    and the first occurrence in each MC file for every MC-histogram."""
    # return reference histograms,
    #        mchistos = mc plots, 
    #        xsec     = generated xsec and its uncertainty,    
    #        Nev      = sum of weights, sum of squared weight, number of events
    # (derived from rivet-cmphistos)
    mchistos = {}
    xsec = {}
    Nev = {}
    # for infile in filelist:
    mchistos.setdefault(filelist, {})
    analysisobjects = yoda.read(filelist)
    contur.config.contur_log.info("Found {} analysisobjects in {}".format(len(analysisobjects),filelist))
    for path, ao in analysisobjects.items():
        if path.startswith('/_EVTCOUNT'):
            Nev = ao
        if path.startswith('/_XSEC'):
            xsec = ao
            # Conventionally don't plot data objects whose names start with an
            # underscore
        if os.path.basename(path).startswith("_"):
            continue
        if rivet.isRefPath(path):
            # Reference histograms are read elsewhere.
            continue
        if rivet.isRawPath(path):
            # Reference histograms are read elsewhere.
            continue
        if rivet.isTheoryPath(path):
            # Theory histograms are read elsewhere.
            continue


        else:
            if path not in mchistos[filelist]:
                mchistos[filelist][path] = ao
    return mchistos, xsec, Nev


def writeHistoDat(mcpath, plotparser, outdir, nostack, histo):
    """Write a .dat file for the histogram in the output directory, for later display."""

    anaobjects = []
    drawonly = []
    mcpath = "/"+mcpath

    if nostack:
        sigback = histo.sigplot
    else:
        sigback = histo.stack

    ## Check if we have reference data for the histogram
    if not histo.ref:
        contur.config.contur_log.warning("Not writing dat file for ".format(histo.signal.path()))
        return

    h = rivet.stripOptions(sigback.path())

    hparts = h.strip("/").split("/")
    hparts[0] = rivet.stripOptions(hparts[0])


    outdir = outdir+"/"+histo.pool+"/"+hparts[0]
    mkoutdir(outdir)

    ratioreference = None

    refdata = histo.refplot
    if histo.thyplot:
        theory = histo.thyplot

    sigback.setAnnotation('Path', mcpath+h)

    refdata.setAnnotation('ErrorBars', '1')
    refdata.setAnnotation('PolyMarkers', '*')
    refdata.setAnnotation('ConnectBins', '0')

    if histo.thyplot:
        theory.setAnnotation('ErrorBars', '1')
        theory.setAnnotation('LineColor', 'green')
        theory.setAnnotation('ErrorBands','1')
        theory.setAnnotation('ErrorBandColor','green')
        theory.setAnnotation('ErrorBandOpacity','0.3')

        anaobjects.append(theory)

    anaobjects.append(refdata)

    drawonly.append('/REF' + h)
    drawonly.append('/THY' + h)
    drawonly.append(mcpath + h)

    # write the bin number of the most significant bin, and the bin number for the plot legend
    if histo.CLs is not None and histo.CLs > 0:
        if histo.conturBucket.index is not None:
            indextag=histo.conturBucket.index
        else:
            indextag="Correlated all"
        sigback.setAnnotation('Title','[%s] %5.2f' % ( indextag, histo.CLs ))
    else :
        sigback.setAnnotation('Title','No exclusion')


    sigback.setAnnotation('LineColor', 'red')
    #sigback.setAnnotation('ErrorBars', '1')

    anaobjects.append(sigback)

    plot = contur.util.Plot()

    for key, val in plotparser.getHeaders(h).items():
        # Get any updated attributes from plotinfo files
        plot[key] = val


    plot['DrawOnly'] = ' '.join(drawonly).strip()
    plot['Legend'] = '1'
    plot['MainPlot'] = '1'
    plot['LogY'] = '1'
    plot['RatioPlot'] = '1'
    plot['RatioPlotSameStyle'] = 0

    #plot['RatioPlotYMin'] = '0.0'
    # plot['RatioPlotMode'] = 'default'    

    plot['RatioPlotMode'] = 'deviation'    
    plot['RatioPlotYMin'] = '-3.0'
    plot['RatioPlotYMax'] = '3.0'
    
    plot['RatioPlotYLabel'] = '(Bkgd+BSM)/Data'

    plot['RatioPlotErrorBandColor'] = 'Yellow'
    
        
    if contur.config.expectedLimit:
        refdata.setAnnotation('Title', 'SM')
        plot['RatioPlotYLabel'] = '(SM+BSM)/SM'
    else:
        refdata.setAnnotation('Title', 'Data')    
        plot['RatioPlotYLabel'] = '(Bkgd+BSM)/Data'

    ratioreference = '/REF'+rivet.stripOptions(h)
    plot['RatioPlotReference'] = ratioreference
    output = ''
    output += str(plot)
    from io import StringIO
    sio = StringIO()
    yoda.writeFLAT(anaobjects, sio)
    output += sio.getvalue()
    outfile = '%s.dat' % hparts[1]
    mkoutdir(outdir)
    outfilepath = os.path.join(outdir, outfile)
    f = open(outfilepath, 'w')
    f.write(output)
    f.close()


def mkScatter2D(s1):
    """ Make a Scatter2D from a Scatter1D by treating the points as y values and adding dummy x bins."""

    rtn = yoda.Scatter2D()

    xval = 0.5
    for a in s1.annotations():
        rtn.setAnnotation(a, s1.annotation(a))

    rtn.setAnnotation("Type", "Scatter2D");

    for point in s1.points():
        
        ex_m = xval-0.5
        ex_p = xval+0.5

        y = point.x()
        ey_p = point.xMax() - point.x()
        ey_m = point.x()    - point.xMin()
        
        pt = yoda.Point2D(xval, y, (0.5,0.5), (ey_p,ey_m))
        rtn.addPoint(pt)
        xval = xval + 1.0

    return rtn

def walklevel(some_dir, level=1):
    """Like os.walk but can specify a level to walk to
    useful for managing directories a bit better

    https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
    """
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


def newlogspace(start, stop, num=50, endpoint=True, base=10.0, dtype=None):
    """Numpy logspace returns base**start to base**stop, we modify this here so it returns logspaced between start and stop"""
    return np.logspace(np.log(start)/np.log(base), np.log(stop)/np.log(base), num, endpoint, base, dtype)

def getLabel(param):
    """Convenience funtion to guess a sensible axis label"""
    if param == "mh2":
        label = '$m_{h_2}$ (GeV)'
    elif param == "sa":
        label = '$\sin \\alpha$'    
    elif param == "mzp":
        label = '$m_{Z^\prime}$'    
    elif param == "g1p":
        label = '$g^\prime_1$'    
    else:
        label = param

    return label


def findParamPoint(fileList,tag,paramList):
    """Given a list of parameters and values, find the appropriate yoda file in a tree and return it.
       If no params given, just return the existing list
    """

    if len(paramList)==0:
        return fileList

    params = {}
    testValues = {}
    fileNames = {}

    # parse the parameter list
    for pair in paramList:
        temp = pair.split('=')
        params[temp[0]]=float(temp[1])
        testValues[temp[0]]=None
        fileNames[temp[0]]=[]
        
    contur.config.contur_log.info('Looking for the closest match to these parameter values: {}'.format(params))

    # scan directory tree(s) looking in the param.dat files for closest values. 
    contur.config.contur_log.info('Looking in {}'.format(fileList))
    for scan_path in fileList:
        for root, dirs, files in sorted(os.walk(scan_path)):
            for file_name in files:
                if file_name == contur.config.paramfile:
                     #param_dict = contur.util.read_param_point(os.path.join(root,file_name))
                    param_dict = contur.util.read_param_point(os.path.join(root,file_name))
                    run_point_num = os.path.basename(root)
                    yoda_file = os.path.join(root, tag + run_point_num + '.yoda.gz')
                    if not os.path.exists(yoda_file):
                        yoda_file = os.path.join(root, tag + run_point_num + '.yoda')
                        if not os.path.exists(yoda_file):
                            contur.config.contur_log.warn("No yoda file found in "+root)

                    for key, value in param_dict.items():

                        if key in params.keys():
                            testValue = abs(value-params[key])
                            # Add the yoda files for nearest values to file list.
                            if testValues[key] is None or testValue < testValues[key]:
                                testValues[key] = testValue
                                fileNames[key] = [yoda_file]
                            elif testValue == testValues[key]:
                                fileNames[key].append(yoda_file)

    # now look for a yoda file which is in the "closest" list for all parameters.
    newList = []
    testP = next(iter(params))
    for fileN in fileNames[testP]:
        inAll = True
        for names in fileNames.values():
            if fileN not in names:
                inAll = False

        if inAll:
            newList.append(fileN)

    contur.config.contur_log.info('These files have been identified as the nearest match: {}'.format(newList))
    return newList
