#!/usr/bin/env python

import os
import shutil
import sys
from builtins import input
import textwrap

import contur
from configobj import ConfigObj

run_key_list = ["generator", "contur"]


def make_directory(path):
    """If directory does not exist, create it."""
    try:
        os.mkdir(path)
    except OSError as os_error:
        if '[Errno 17] File exists' not in str(os_error):
            raise os_error


def sanitise_inputs_floats(section, key):
    """
    special config obj sanitizer for the smaller param dat files to just take
    everything to floats
    """
    try:
        section[key] = float(section[key])
    except:
        return


def write_sampled_map(output_dir, param_dict):
    # sampled_points = {}
    sampled_points = ConfigObj()
    sampled_points["keys"] = param_dict.keys()
    sampled_points["values"] = {}
    for k, v in param_dict.items():
        sampled_points["values"][k] = list(v["values"])
    sampled_points["points"] = {}

    for root, _, files in os.walk(output_dir):
        for file_name in files:
            if file_name == contur.config.paramfile:
                run_point = os.path.basename(root)
                param_file_dict = contur.util.read_param_point(
                    os.path.join(root, file_name))
                sampled_points["points"][run_point] = param_file_dict
    sampled_points.filename = os.path.join(output_dir, 'sampled_points.dat')
    sampled_points.write()


def read_sampled_map(input_dir):
    # read the samples file
    sampled_points = ConfigObj(os.path.join(input_dir, "sampled_points.dat"))
    # sampled_points.walk(sanitise_inputs_map, call_on_sections=True)
    return sampled_points


def write_sampled_points(output_dir):
    """Write where parameter space was sampled to a .txt file"""

    sampled_points = {}
    for root, _, files in os.walk(output_dir):
        for file_name in files:
            if file_name == contur.config.paramfile:
                run_point = os.path.basename(root)
                param_file_dict = contur.util.read_param_point(
                    os.path.join(root, file_name))
                sampled_points[run_point] = param_file_dict

    variables = [key for key in sorted(param_file_dict)]
    with open(os.path.join(output_dir, 'sampled_points.dat'), 'w') as f:
        # Write headers
        f.write('run_point: \t')
        [f.write(variable + ': \t') for variable in variables]
        f.write("\n")
        # Write data points
        for run_point, param_dict in sorted(sampled_points.items()):
            f.write(run_point + '\t')
            for variable in variables:
                f.write("%.4f \t" % param_dict[variable])
            f.write('\n')


def sanitise_inputs(section, key):
    """Function specific to config obj to walk and convert key values."""

    # list of keys to convert from strings to ints/floats
    key2float = ["start", "stop", "value"]
    key2int = ["number"]
    key2list = run_key_list
    if key in key2list:
        # function to make single entries in otherwise csv lists into a list
        # for easier processing
        if not type(section[key]) == list:
            section[key] = [section[key]]
    if key in key2float:
        section[key] = float(section[key])
    if key in key2int:
        section[key] = int(section[key])


def get_exclusions(param_path):
    """
    Get points that we will exclude from param file
    """
    config = ConfigObj(param_path)
    if 'Exclusions' in config.keys():
        return [int(i) for i in str.split(config['Exclusions']['points'])]
    else:
        return []


def read_param_steering_file(param_file):
    """Read in the parameter card and convert to a python dict"""

    config = ConfigObj(param_file)
    config.walk(sanitise_inputs, call_on_sections=True)
    block_list = ["Parameters", "Run"]

    for key in block_list:
        if key not in config.keys():
            contur.config.contur_log.error(
                "Error reading {}".format(param_file))
            raise KeyError(
                "Input parameter file must contain {} block".format(key))

    param_dict = config["Parameters"]
    run_dict = config["Run"]
    run_args = run_key_list

    for run_arg in run_args:
        if run_arg not in run_dict.keys():
            print("Warning: No %s setup strings specified, "
                  "batch executed commands may not work\n" % run_args)
        else:
            for path in run_dict[run_arg]:
                if not os.path.exists(path):
                    print("Warning: For %s setup,  %s does not exist!\n" %
                          (run_arg, path))
                    if not permission_to_continue("Do you wish to continue?"):
                        sys.exit()

    for param_name, param_config_dict in param_dict.items():
        allowed_modes = {
            "LOG": {"start", "stop", "number"},
            "LIN": {"start", "stop", "number"},
            "CONST": {"value"},
            "REL": {"form"},
            "DIR": {"name"},
            "SINGLE": {"name"},
            "DF": {"name"}
        }

        def fmt_iter(iterable):
            if len(iterable) == 0:
                return "(None)"
            return ", ".join(iterable)

        try:
            param_mode = param_config_dict["mode"]
        except KeyError:
            error_message = f"""
            All parameters in {param_file} must have a mode assigned.
            Choose from either: {fmt_iter(allowed_modes.keys())}
            """
            raise ValueError(textwrap.dedent(error_message))
        try:
            required_param_keys = allowed_modes[param_mode]
        except KeyError:
            raise ValueError(f"Param mode \"{param_mode}\" not allowed. Please, "
                             f"choose from {fmt_iter(allowed_modes.keys())}")

        param_keys = set(param_config_dict.keys())

        # We spare from including "mode" in every entry in the dict
        # `allowed_modes`, as it is obvious that each mode must include the
        # mode key. However, when checking its presence, we need to include it.
        complete_req_param_keys = {*required_param_keys, "mode"}
        if param_keys != complete_req_param_keys:
            error_message = f"""
            In parameter file "{param_file}", parameter block "{param_name}", 
            mode "{param_mode}" requires keys: {fmt_iter(complete_req_param_keys)}.
                Missing keys: {fmt_iter(complete_req_param_keys - param_keys)}.
                Disallowed keys: {fmt_iter(param_keys - complete_req_param_keys)}.
            """
            raise ValueError(textwrap.dedent(error_message))

    return param_dict, run_dict


def make_run_point_directory(run_point, output_dir):
    """If run point directories don't exist, make them and return path"""
    run_point_dir_name = "%04i" % run_point
    run_point_path = os.path.join(output_dir, run_point_dir_name)
    make_directory(run_point_path)
    return run_point_path


def write_param_file(param_dict, run_point_path, run_point):
    """Write param file containing parameter values for given run point"""
    run_point_param_file_path = os.path.join(run_point_path,
                                             contur.config.paramfile)
    with open(run_point_param_file_path, 'w') as run_point_param_file:
        for param, info in sorted(param_dict.items()):
            value = info['values'][run_point]
            run_point_param_file.write("%s %e\n" % (param, value))


def write_param_dat(param_dict, run_point_path, run_point):
    config = ConfigObj()
    for param, info in sorted(param_dict.items()):
        value = info['values'][run_point]
        config[param] = info["values"][run_point]
    config.filename = os.path.join(run_point_path, contur.config.paramfile)
    config.write()

    if 'slha_file' in param:
        run_point_param_file_path = os.path.join(run_point_path,
                                                 contur.config.paramfile)
        block_list = {"MASS"}
        masses = contur.util.read_slha_file(run_point_path, [value], block_list)
        new = ''
        value_path = os.path.join(run_point_path, value)
        with open(value_path, 'r') as outfile, open(run_point_param_file_path,
                                                    'w') as infile:
            for particle, mass in masses.items():
                new = particle + ' = ' + str(mass) + '\n'
                infile.write(new)


def gen_format_dict(parameters, idx):
    """Create dictionary to use in formatting template run card."""
    format_dict = {}
    for param, info in sorted(parameters.items()):
        format_dict[param] = info['values'][idx]
    return format_dict


def write_template_files(templates, param_dict, run_point, run_point_path, args,
                         beam):
    """Write template files formatted with parameter values"""

    for template_name in templates:
        raw_template_text = templates[template_name]
        format_dict = gen_format_dict(param_dict, run_point)
        try:
            template_text = raw_template_text.format(**format_dict)
        except KeyError:
            print("Error: Parameters in %s do not match the parameters in %s."
                  % (args.param_file, template_name))
            sys.exit()

        # Commands for specific generators added here with conditionals on
        # the config.
        if contur.config.mceg == "herwig":

            # Herwig-specific stuff
            template_text += "set EventGenerator:EventHandler:LuminosityFunction:Energy " + \
                             str(filter(str.isdigit, str(beam))) + "000 \n"

            if args.pipe_hepmc:

                template_text += "read snippets/HepMC.in \n"
                template_text += "set /Herwig/Analysis/HepMC:PrintEvent 10000000 \n"

            else:

                template_text += "create ThePEG::RivetAnalysis Rivet RivetAnalysis.so \n"
                template_text += "insert EventGenerator:AnalysisHandlers 0 Rivet \n"
                template_text += "read " + beam + ".ana \n"

            template_text += "saverun LHC EventGenerator \n"

        elif contur.config.mceg == "madgraph":

            template_text += "set ebeam1 %i\n" % (
                        float(beam.strip("TeV")) * 1000 / 2)
            template_text += "set ebeam2 %i\n" % (
                        float(beam.strip("TeV")) * 1000 / 2)
            template_text += "set iseed %i\n" % args.seed
            template_text += "set nevents %i\n" % args.num_events

            if args.pipe_hepmc:
                template_text += "set HEPMCoutput:file fifo\n"

        template_path = os.path.join(run_point_path, template_name)
        with open(template_path, 'w') as f:
            f.write(template_text)


class WorkingDirectory:
    """Context manager to temporarily change working directory"""

    def __init__(self, temp_working_directory):
        self.temp_working_directory = os.path.abspath(temp_working_directory)

    def __enter__(self):
        self.working_directory = os.getcwd()
        os.chdir(self.temp_working_directory)

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.chdir(self.working_directory)


def permission_to_continue(message):
    """Get permission to continue program"""
    permission = ''
    while permission.lower() not in ['no', 'yes', 'n', 'y']:
        permission = str(input(message + '\n[y/N]: '))
        if permission == '':
            permission = 'N'
    if permission.lower() in ['y', 'yes']:
        return True
    else:
        return False


def delete_files(root):
    """delete all the superfluous files in root"""
    for delete_file in contur.config.unneeded_files:
        try:
            file_path = os.path.join(root, delete_file)
            if os.path.isdir(file_path):
                shutil.rmtree(file_path)
            else:
                os.remove(os.path.join(root, delete_file))
        except OSError:
            contur.config.contur_log.debug(' {} not found'.format(
                os.path.join(root, delete_file)))
