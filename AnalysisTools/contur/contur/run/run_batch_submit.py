#!/usr/bin/env python

import os
import subprocess
import numpy as np
import argparse
from shutil import copyfile
from configobj import ConfigObj

import contur


def get_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(description=(
        "Run a parameter space scan and submit batch jobs.\n"
        "Produces a directory for each beam containing generator config file detailing the "
        "parameters used at that run point and a shell script to run the generator "
        "that is then submitted to batch.\n"
        "\n"), formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # Optional arguments
    parser.add_argument("-o", "--out_dir", dest="out_dir", type=str,
                        metavar="output_dir", default="myscan00",
                        help="Specify the output directory name.")
    parser.add_argument('-p', '--param_file', dest='param_file', type=str,
                        default='param_file.dat', metavar='param_file',
                        help='File specifying parameter space.')
    parser.add_argument('-t', '--template', dest='template_file',
                        default=contur.config.mceg_template, metavar='template_file',
                        help='Template Herwig .in file.')
    parser.add_argument("-g", "--grid", dest="grid_pack", type=str,
                        default='GridPack', metavar='grid_pack',
                        help=("Provide additional grid pack. Set to 'none' to not use one."))
    parser.add_argument("-n", "--numevents", dest="num_events",
                        default='30000', metavar='num_events', type=int,
                        help="Number of events to generate.")
    parser.add_argument('--seed', dest='seed', metavar='seed', default=101,
                        type=int, help="Seed for random number generator.")
    parser.add_argument("-q", "--queue", dest="queue", metavar="queue", default="",
                        type=str, help="batch queue.")
    parser.add_argument('-s', '--scan_only', dest='scan_only', default=False,
                        action='store_true', help='Only perform scan and do not submit batch job.')
    parser.add_argument('-b', '--beams', dest="beams", default="all",
                        type=str, help="beam.")
    parser.add_argument('-P', '--pipe_hepmc', dest="pipe_hepmc", default=False,
                        action='store_true', help="Rivet reading from pipe.")
    parser.add_argument('-w', '--wallTime', type=str, default=None,
                        help="Set maximum wall time for jobs (HH:MM).")
    parser.add_argument('--memory', type=str, default=None,
                        help="Set maximum memory consumption for jobs (e.g. 2G).")
    parser.add_argument('-B', '--batch', dest="batch_system", default='qsub',
                        type=str, metavar='batch_system',
                        help="Specify which batch system is using, support: qsub, condor or slurm")
    parser.add_argument("-m", "--mceg", dest="mceg", metavar="mceg", default=contur.config.mceg,
                        type=str, help="MC event generator.")
    parser.add_argument('-v','--variable',action='store_true',
                      help='Use this flag to make number of events for each point variable')

    args = parser.parse_args()

    return args


def valid_arguments(args):
    """
    Check that command line arguments are valid; return True or False.
    This function is also responsible for formatting some arguments e.g.
    converting the GridPack path to an absolute path and checking it contains .ana files."""
    valid_args = True

    beams = []
    known_beams = contur.data.getBeams()
    if args.beams == "all":
        beams = known_beams
    else:
        try_beams = args.beams.split(",")
        for beam in try_beams:
            if beam in known_beams:
                beams.append(beam)
            else:
                print("Beam '%s' is not known. Possible beams are:" % beam)
                for beam in known_beams:
                    print(beam)
                valid_args = False

    if not os.path.exists(args.param_file):
        print("Param file '%s' does not exist!" % args.param_file)
        valid_args = False

    if not os.path.exists(args.template_file):
        print("Template file '%s' does not exist!" % args.template_file)
        valid_args = False

    if args.grid_pack.lower() == 'none':
        args.grid_pack = None
    else:
        args.grid_pack = os.path.abspath(args.grid_pack)
        if not os.path.isdir(args.grid_pack):
            print("No such grid pack directory '%s'!" % args.grid_pack)
            valid_args = False
        for beam in beams:
            afile = beam+".ana"
            if not os.path.exists(os.path.join(args.grid_pack, afile)):
                print("Copying "+os.path.join(os.environ['CONTURMODULEDIR'],
                                              "AnalysisTools/GridSetup/GridPack", afile) + " to " + os.path.join(args.grid_pack, afile))
                copyfile(os.path.join(os.environ['CONTURMODULEDIR'], "AnalysisTools/GridSetup/GridPack", afile),
                         os.path.join(args.grid_pack, afile))

    try:
        int(args.num_events)
    except ValueError:
        print("Number of events '%s' cannot be converted to integer!"
              % args.num_events)
        valid_args = False

    try:
        args.seed = int(args.seed)
    except ValueError:
        print("Seed '%s' cannot be converted to integer!" % args.seed)
        valid_args = False

    if args.wallTime is not None:
        timespans = args.wallTime.split(":")
        if len(timespans) != 2:
            print("Have to give max wall time in the format <hh:mm>!")
            valid_args = False
        else:
            try:
                for span in timespans:
                    span = int(span)
                    if span >= 60:
                        print(
                            "Have to give time spans of less than 60 [units]!")
                        valid_args = False
            except ValueError:
                print("Have to give time spans that can be converted to integers!")
                valid_args = False

    if args.memory is not None:
        number, unit = args.memory[0:-1], args.memory[-1]
        valid_units = ["M", "G"]
        if unit not in valid_units:
            print("'%s' is not a valid unit for the memory. (%s are valid units.)" % (
                unit, valid_units))
            valid_args = False
        if not number.isdigit():
            print("'%s' is not a valid number for the memory." % number)
            valid_args = False

    if not (args.mceg == "herwig" or args.mceg == "madgraph" or args.mceg == "pbzpwp"):
        print("Unrecognised event generator: {}".format(args.mceg))
        valid_args = False
    else:
        contur.config.mceg = args.mceg
        contur.config.mceg_template = args.template_file

    return valid_args, beams


def gen_herwig_commands(directory_name, grid_pack, pipe_hepmc, seed, num_events, runbeam):

    # Create Herwig run card from LHC.in. Pass it the full grid pack
    # directory path to read model files from
    herwig_commands = ('Herwig read %s -I %s -L %s;\n' %
                       (contur.config.mceg_template, grid_pack, grid_pack))
    # Run Herwig run card LHC.run
    run_card_name = os.path.splitext(contur.config.mceg_template)[0] + '.run'

    if pipe_hepmc:
        filestem = 'LHC-S' + str(seed) + "-" + contur.config.tag + directory_name
        herwig_commands += ('mkfifo '+filestem+'.hepmc; \n')
        herwig_commands += ('Herwig run ' + run_card_name +
                            ' --seed=' + str(seed) +
                            ' --tag=' + contur.config.tag + directory_name +
                            ' --numevents=' + str(num_events) + '&\n')
        herwig_commands += ('rivet -a $RA'+runbeam+' -n '+str(num_events) +
                            ' -o '+filestem+'.yoda '+filestem+'.hepmc \n')

    else:
        herwig_commands += ('Herwig run ' + run_card_name +
                            ' --seed=' + str(seed) +
                            ' --tag=' + contur.config.tag + directory_name +
                            ' --numevents=' + str(num_events) + ';\n')

    return herwig_commands


def gen_madgraph_commands(directory_name, grid_pack, pipe_hepmc, seed, num_events, runbeam):
    run_card_name = contur.config.mceg_template
    madgraph_commands = ""

    output_directory = "mgevents/Events/run_01/"

    if pipe_hepmc:
        madgraph_commands += ('$MG_EXEC ' + run_card_name + ' &\n')
        hepmc_file_name = os.path.join(output_directory, "PY8.hepmc.fifo")
        madgraph_commands += ('while [[ ! -p '+hepmc_file_name +
                              ' ]]; do sleep 1; done # wait for creation of fifo\n')

    else:
        madgraph_commands += ('$MG_EXEC ' + run_card_name + '\n')
        hepmc_file_name = os.path.join(
            output_directory, "tag_1_pythia8_events.hepmc.gz")

    madgraph_commands += ('rivet --skip-weights' +
        ' -a $RA' + runbeam +
        ' -n ' + str(num_events) +
        ' -o LHC-S' + str(seed) + '-' + contur.config.tag + directory_name + '.yoda ' +
        hepmc_file_name + '\n')
    return madgraph_commands


def gen_pbzpwp_commands(directory_name, grid_pack, num_events, runbeam):

    run_card_name = contur.config.mceg_template
    pbzpwp_commands = ('mkdir output.pbzpwp \n')
    pbzpwp_commands += ('cp ../../../pbzp_input_contur.py . \n')
    pbzpwp_commands += ('./pbzp_input_contur.py \n')
    pbzpwp_commands += ('cd output.pbzpwp \n')
    pbzpwp_commands += ('> Timings.txt \n')
    pbzpwp_commands += ('mv ../powheg.input . \n')
    pbzpwp_commands += ('sed -i "s/.*! The number of events*/numevts     ' +
                        str(num_events)+' ! The number of events/" powheg.input \n')
    pbzpwp_commands += ('sed -i "s/.*! The energy of beam one*/ebeam1 '+str(
        int(runbeam[:-3])*1000.0/2.0)+'    ! The energy of beam one/" powheg.input \n')
    pbzpwp_commands += ('sed -i "s/.*! The energy of beam two*/ebeam2 '+str(
        int(runbeam[:-3])*1000.0/2.0)+'    ! The energy of beam two/" powheg.input \n')
    pbzpwp_commands += ('cp ../../../../pwhg_main . \n')
    pbzpwp_commands += ('(echo -n pwhg_main start \' \' ; date ) >> Timings.txt \n')
    pbzpwp_commands += ('./pwhg_main > output-pbzpwp.log \n')
    pbzpwp_commands += ('(echo -n pwhf_main end \' \' ; date ) >> Timings.txt \n')
    return pbzpwp_commands


def gen_pbzpwppy8_commands(directory_name, grid_pack, num_events, runbeam):
    pbzpwppy8_commands = ('mkfifo my_fifo \n')
    pbzpwppy8_commands += ('cp ../../../main-pythia . \n')
    pbzpwppy8_commands += ('(echo -n pythia-rivet start \' \' ; date ) >> Timings.txt \n')
    pbzpwppy8_commands += ('./main-pythia my_fifo output.pbzpwp/pwgevents.lhe ' +
                           str(num_events)+' & \n')
    pbzpwppy8_commands += ('rivet -a $RA'+runbeam+' -o ' +
                           contur.config.tag+directory_name+'.yoda my_fifo \n')
    pbzpwppy8_commands += ('(echo -n pythia-rivet end  \' \' ; date ) >> Timings.txt \n')
    return pbzpwppy8_commands


def gen_batch_command(directory_name, directory_path, args, setup_commands, runbeam="13TeV"):
    """Generate commands to write to batch file"""
    batch_command = ''

    batch_command += ('#! /bin/bash\n' +
                      '#$ -j y # Merge the error and output streams into a single file\n' +
                      '#$ -o '+directory_path+'/contur.log\n')

    if contur.config.mceg == "herwig":
        # Setup the runtime environment
        for l in setup_commands['generator']:
            batch_command += "source " + l + ';\n'

        # Setup Contur environment
        for l in setup_commands['contur']:
            batch_command += "source " + l + ';\n'

        # Change directory to run point folder
        batch_command += 'cd ' + directory_path + ';\n'

        # Add Herwig-specific commands
        batch_command += gen_herwig_commands(
            directory_name, args.grid_pack, args.pipe_hepmc, args.seed, args.num_events, runbeam)

    if contur.config.mceg == "pbzpwp":
        # Setup PBZp environment
        batch_command += "source " + setup_commands['generator'][0] + ';\n'

        # Change directory to run point folder
        batch_command += 'cd ' + directory_path + ';\n'

        # PBZp commands
        batch_command += gen_pbzpwp_commands(directory_name,
                                             args.grid_pack, args.num_events, runbeam)

        # Setup the runtime environment
        batch_command += "source " + setup_commands['generator'][1] + ';\n'

        # Setup Contur environment
        for l in setup_commands['contur']:
            batch_command += "source " + l + ';\n'

        # Change directory to run point folder
        batch_command += 'cd ' + directory_path + ';\n'

        # Pythia commands
        batch_command += gen_pbzpwppy8_commands(
            directory_name, args.grid_pack, args.num_events, runbeam)

    elif contur.config.mceg == "madgraph":
        # Setup the runtime environment
        for l in setup_commands['generator']:
            batch_command += "export MG_EXEC=" + l + ';\n'

        # Setup Contur environment
        for l in setup_commands['contur']:
            batch_command += "source " + l + ';\n'

        # Change directory to run point folder
        batch_command += 'cd ' + directory_path + ';\n'

        # Madgraph-specific commands
        batch_command += gen_madgraph_commands(
            directory_name, args.grid_pack, args.pipe_hepmc, args.seed, args.num_events, runbeam)

    batch_filename = contur.config.tag + directory_name + '.sh'

    # if using Condor: generate a job description language file to accompany the script(s)
    # Step:
    # Generate JDL file, one for each individual job execution
    # Generate the job execution scripts as usual
    # Submit the JDL file instead the job script to the hex machine at ppe instead of the qsub systems using any machine
    # In the meantime, need to add a flag to the batch_submit functionality to select which batch system is using HTCondor or qsub
    # return a list of [content,filename] pairs, to support JDL submission systems

    if contur.config.using_condor:
        job_description_file_content = ''
        job_description_file_content += "#!/bin/bash" + '\n'
        job_description_file_content += "universe = vanilla"+'\n'
        job_description_file_content += "executable = " + \
            contur.config.tag + directory_name + '.sh' + '\n'
        job_description_file_content += "log = " + \
            contur.config.tag + directory_name + '.sh.log' + '\n'
        job_description_file_content += "requirements = " + ' OpSysAndVer=="SL6" ' + '\n'
        job_description_file_content += "output = " + \
            contur.config.tag + directory_name + '.sh.out' + '\n'
        if args.wallTime is not None:
            # convert unit
            hours, minutes = args.wallTime.split(":")
            minutes = 60*int(hours) + int(minutes) # add hours to minutes
            job_description_file_content += "maxWallTime = %s # min\n" % minutes
        if args.memory is not None:
            # convert unit
            number, unit = args.memory[0:-1], args.memory[-1]
            if unit == "G":
              number = int(number)*1000 # convert GB to MB
            job_description_file_content += "requestMemory = %s # MB \n" % number
        job_description_file_content += "queue" + '\n'


        job_description_file_name = contur.config.tag + directory_name + '.job'

        return batch_command, batch_filename, job_description_file_content, job_description_file_name

    return batch_command, batch_filename


def gen_submit_command(queue, wallTime=None, memory=None):

    if contur.config.using_slurm:
        qsub = "sbatch"
        if queue != "":
            qsub += " -p "+queue
        else:
            qsub += " -p RCIF"
        qsub += " -e contur.log -o contur.log"
        if wallTime is not None:
            qsub += " -t %s" % wallTime
        if memory is not None:
            # convert unit
            number, unit = memory[0:-1], memory[-1]
            if unit == "G":
              number = int(number)*1000 # convert GB to MB
            qsub += " --mem=%s" % number
    elif contur.config.using_qsub:
        qsub = "qsub"
        if queue != "":
            qsub = qsub + " -q "+queue
        if wallTime is not None:
            # make h_rt slightly longer than s_rt to allow job to react to soft kill
            qsub += " -l h_rt="+wallTime+":10 -l s_rt="+wallTime+":00"
        if memory is not None:
            qsub += " -l h_rss=%s" % memory
    elif contur.config.using_condor:
        qsub = "condor_ce_submit"

    return qsub


def batch_submit(args, beams):
    """Run parameter scan and submit shell scripts to batch"""

    contur.config.using_condor = (args.batch_system == 'condor')
    contur.config.using_slurm = (args.batch_system == 'slurm')
    contur.config.using_qsub = not (
        contur.config.using_condor or contur.config.using_slurm)

    # Make sure scan is not overwriting previous scans
    if os.path.isdir(args.out_dir):
        out_dir_basename = args.out_dir[:-2]
        counter = 1
        while os.path.isdir(args.out_dir):
            args.out_dir = out_dir_basename + "%02i" % counter
            counter += 1

    np.random.seed(args.seed)

    qsub = gen_submit_command(args.queue, args.wallTime, args.memory)

    # Param dict has parameter names as keys and then each item is a
    # dictionary with keys 'range' and 'values'
    param_dict, run_dict = contur.scan.read_param_steering_file(args.param_file)

    contur.scan.check_param_consistency(param_dict, args.template_file)

    # Generate parameter values depending on sampling mode
    param_dict, num_points = contur.scan.generate_points(param_dict)

    # Get exceptions from based on low-movement points
    exclusions = contur.scan.get_exclusions(args.param_file)

    # Create run point directories
    contur.scan.run_scan(param_dict, beams, num_points, args, exclusions)

    # Get variable scale for number of events for each point
    if args.variable:
        scales_str = ConfigObj(args.param_file)['Scales']['points']
        scales = [float(i) for i in scales_str.split()]
        num_events_orig = args.num_events

    for beam in beams:
        beam_directory = os.path.join(args.out_dir, beam)
        for directory_name in os.listdir(beam_directory):

            # If event numbers are variable, scale by values in param file
            if args.variable:
                # This will locate correct point index based on directory name
                try:
                    scale = scales[int(directory_name)]
                # This is to handle non-number directory or other file
                except:
                    scale = 1.0
                args.num_events = int(num_events_orig * scale)


            directory_path = os.path.abspath(
                os.path.join(beam_directory, directory_name))
            if os.path.isdir(directory_path):
                if not contur.config.using_condor:
                    command, filename = gen_batch_command(
                        directory_name, directory_path, args, run_dict, runbeam=beam)
                    batch_command_path = os.path.join(directory_path, filename)
                # generate job description language file name for HTCondor batch
                else:
                    command, filename, job_description_file_content, job_description_file_name = gen_batch_command(
                        directory_name, directory_path, args, run_dict, runbeam=beam)

                    job_description_file_path = os.path.join(
                        directory_path, job_description_file_name)
                    batch_command_path = os.path.join(directory_path, filename)

                # Write batch file command (commands to run Herwig)
                with open(batch_command_path, 'w') as batch_file:
                    batch_file.write(command)

                if contur.config.using_condor:
                    with open(job_description_file_path, 'w') as job_description_file:
                        job_description_file.write(
                            job_description_file_content)

                if args.scan_only is False:
                    print("Submitting: " + batch_command_path)
                    with contur.scan.WorkingDirectory(directory_path):
                        # Changing working directory is necessary here since
                        # qsub reports are outputted to current working directory
                        if not contur.config.using_condor:
                            subprocess.call([qsub + " " + batch_command_path],
                                            shell=True)
                        else:
                            subprocess.call([condor_sub + " " + job_description_file_path],
                                            shell=True)
                        # Note: this needs to be submitted using the 'job submit' machine only
                else:
                    print("Not Submitting: " + qsub + " " + batch_command_path)
