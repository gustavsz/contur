import contur as ct
from optparse import OptionParser

import contur as ct
# this is just used to check if a histo is a valid contur histo, this should be
# factored into histFact and taken out
from contur import data as tf
from contur.scan import os_functions as osf
from optparse import OptionParser
from copy import copy
import contur.util as util
from contur.scan.os_functions import read_param_dat
import sys, os, pickle
import subprocess

parser = OptionParser(usage=__doc__)
parser.add_option("-o", "--outputdir", dest="OUTPUTDIR",
                  default="plots", help="Specify output directory for output "
                                        "plots.")
parser.add_option("-g", "--grid", dest="GRID", default=False,
                  help="specify a folder containing a structured grid of points"
                       " to analyse. Usually 'myscan'.")

def herwigLogRead(logfile):
    #custom code to inject to read a log file, returns a number
    totalwidth=0.0
    ctau=0.0
    x=None
    #import numpy as np
    #x=np.nan
    with open(logfile,mode="rb") as f:
        for line in f.readlines():
            if ("nH1" in line) and ("Width" in line):
                tempstr=line
                var = float(tempstr.split(":")[3])
                #(1.0/var)
                x = 0.1973 * 10 ** (-15.0) * (1 / var)
            # if "H1->Zp,Zp" in line:
            #     tempstr=line
            #
            #     #for partial width reading
            #     x=float(tempstr.split()[1])
            #
            #     #for total width reading:
            #     #ts=tempstr.strip("\n").split(":")
            #     #totalwidth = float(ts[3])
            # elif "H1->Zp," in line:
            #     tempstr=line
            #     y=float(tempstr.split()[1])
            #     if x:
            #         x+=y
            #     else:
            #         x=y
    if not x:
        print "Could not find requested substring in: "+logfile
        x=0.0
        #x=0.00000001
#     tempstr=subprocess.Popen('grep -i "nH1.*Width" ' + logfile, stdout = subprocess.PIPE, shell = True).communicate()[0]
#     tempstr.strip("\n")
#     var=float(tempstr.split(":")[3])
#     ctau = 0.1973 * 10 ** (-15.0) * (1 / var)
# #    widthstring=os.system('grep -i "Zp.*Width" ' + logfile)
    return x

def analyse_grid(scan_path, conturDepot):
    #grid_points = []
    log_counter = 0
    for root, dirs, files in os.walk(scan_path):
        for file_name in files:
            valid_log_file = (file_name.endswith('-1.log') and
                               'LHC' in file_name and
                               'EvtGen' not in file_name)
            if valid_log_file is True:
                log_counter += 1
                log_file_path = os.path.join(root, file_name)
                param_file_path = os.path.join(root, 'params.dat')
                params = read_param_dat(param_file_path)
                #print('\nFound valid log file ' + log_file_path.strip('./'))
                #print('Sampled at:')
                #for param, val in params.iteritems():
                #    print(param + ': ' + str(val))

                parsedlog=herwigLogRead(log_file_path)
                # Perform analysis
                #contur_depot = analyseYODA(yoda_file_path)
                #contur_depot.params = params
                conturDepot.addCustomParamPoint(param_dict=params, logfile=parsedlog)


if __name__ == "__main__":
    # Command line parsing
    # yodafiles = Grid for heat maps or yoda files for single analysis
    opts, yodafiles = parser.parse_args()


    if not opts.GRID:
        sys.stderr.write("Error: Must specify a grid to parse log files on\n")
        sys.exit(1)

    util.writeBanner()

    contur = ct.conturDepot(TestMethod=None)

    if opts.GRID:
        contur.tagGrid(osf.read_sampled_map(opts.GRID))
        analyse_grid(os.path.abspath(opts.GRID), contur)

    contur.buildMapAxis()
    from contur.plot import *
    ctrPlot = ConturPlotBase(contur, outPath='.')
    ctrPlot.buildSpecialGrid("mzp", "g1p")

    import numpy.ma as ma

    #grid=ma.masked_invalid(ctrPlot.conturGrid)
    grid=ctrPlot.conturGrid
    xaxis=ctrPlot.mapAxis["mzp"]
    yaxis=ctrPlot.mapAxis["g1p"]

    import matplotlib.pyplot as plt
    import matplotlib.colors as colors

    fig = plt.figure(figsize=fig_dims)
    # fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(1, 1, 1)

    colors.Normalize(clip=False)
    #we have to use symlognorm to allow zero values, we set the linear range to the SM Higgs Width/100
    CS = plt.pcolormesh(xaxis, yaxis, grid.T, cmap=plt.cm.viridis, vmin=np.nanmin(grid[np.nonzero(grid)]),vmax=np.nanmax(grid),norm=colors.LogNorm(vmin=np.nanmin(grid[np.nonzero(grid)]),vmax=np.nanmax(grid)),snap=True)

    #CS2 = plt.contour(xaxis,yaxis,grid.T,levels=[1e-6,1e-4,1e-2,1],colors="w",labels=[1e-6,1e-4,1e-2,1])
    import matplotlib.ticker as tk

    levels = [0.001, 100]

    CS2 = plt.contour(xaxis, yaxis, grid.T, levels=levels, label="ATLAS Detector", alpha=1.0, colors="w", linestyles="dotted")

    fmt = tk.LogFormatterMathtext()
    #manual_locations = [(10, 0.5), (-0.62, -0.7), (-2, 0.5), (0.5, 1.2)]
    #plt.clabel(CS2, inline=0, fontsize=8,fmt=fmt)#,inline_spacing=0)#,manual=manual_locations)
                        #,norm=colors.SymLogNorm(linthresh=0.001,vmin=0.001,vmax=np.nanmax(grid)))#, norm=colors.LogNorm(vmin=np.nanmin(grid),vmax=np.nanmax(grid)), snap = True)

                        #, norm=colors.LogNorm(vmin=np.nanmin(grid),vmax=np.nanmax(grid)), snap = True)
    plt.ylabel(r"$g'_{1}$")
    plt.xlabel(r"$M_{Z'}$ [GeV]")
    ax.set_yscale("log", nonposy='clip')
    ax.set_xscale("log", nonposy='clip')
    #ax.set_xticks([1,10,100,1000,10000], minor=False)
    cbar = plt.colorbar(CS)
    cbar.set_label(r"$c\tau ({N_{1}} ) $ [m]")
    cbar.add_lines(CS2, erase=False)
    fig.tight_layout(pad=0.15)


    plt.savefig("N1ctau.pdf")
    plt.savefig("N1ctau.png")

    #ctrPlot.buildAxesfromSpecialGrid(xarg="mzp",logX=True,yarg="g1p",logY=True,logZ=True)

    print "done"