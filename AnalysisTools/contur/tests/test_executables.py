try:
    import contur,rivet,yoda
except:
    raise ImportError("Exiting test suite, could not find the dependencies of YODA, Rivet or contur in PYTHONPATH")

import os
import shutil

import pytest
import yaml

test_dir = os.path.dirname(os.path.abspath(__file__))
#exec_dir = os.path.abspath(os.path.join(test_dir, '../../bin/contur'))

from contur.run.run_analysis import main,get_args

args_path = os.path.join(test_dir, 'sources/grid_cl_args.yaml')
with open(args_path, 'r') as f:
    arguments_examples = yaml.load(f)


def build_executable_cmd(cl_args_dict):
    cl_string=[cl_args_dict["command"]]
    try:
        cl_string.append(cl_args_dict["args"])
    except:
        pass
    try:
        for k,v in cl_args_dict["options"].items():
            #load the optional args to a string
            cl_string.append("--%s=%s" % (k,v))
    except:
        pass
    try:
        for v in cl_args_dict["switches"]:
            #load the optional switches to the string
            cl_string.append("-%s" % v)
    except:
        pass

    return cl_string


main_run_cmds={}

for k,v in arguments_examples.items():
    cmd=build_executable_cmd(v)
    main_run_cmds[k]=get_args(cmd[1:])

# @pytest.mark.first
@pytest.mark.parametrize("fixture",main_run_cmds.values(),ids=main_run_cmds.keys())
def test_run_main(fixture):
    main(fixture)


#@pytest.mark.second_to_last

'''
#For now lets comment this out so at a basic level the tests run the executables once but nothing fancy

def test_calculations():
    target_path = os.path.join(test_dir, "sources/contur.map")
    performed_path = os.path.join(test_dir, "tmp/single/contur.map")

    with open(target_path, 'rb') as f:
        file_target = pickle.load(f)
        target = {}
        for bucket in file_target.conturDepotInbox[0].yodaFactory.sortedBuckets:
            target[bucket.pools] = (bucket.CLs,bucket.tags)
    with open(performed_path, 'rb') as f:
        file_perf = pickle.load(f)
        perf = {}
        for bucket in file_perf.conturDepotInbox[0].yodaFactory.sortedBuckets:
            perf[bucket.pools] = (bucket.CLs,bucket.tags)

    for k,v in perf.items():
        print "Reference value %s, Reference histoIDs %s, Reference bucket %s" % (str(target[k][0]), target[k][1], k )
        print "Generated value %s, Generated histoIDs %s, Generated bucket %s \n" % (str(v[0]), v[1], k )
        assert numpy.isclose(target[k][0],v[0])
    return
'''

#@pytest.mark.last
#def teardown_module():
#    """Clean up test area"""
#    shutil.rmtree(os.path.join(test_dir, 'tmp'))
