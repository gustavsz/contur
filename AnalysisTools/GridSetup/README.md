# Contents

## LHC.in

Example input file for Herwig. Works with the DM model in Models/DM/DM_vector_mediator_HF_UFO

## param_file.dat

Example parameter file, works with LHC.in

## tests. 

Currently obsolete. TODO: reinstate these.

## GridPack

This directory is now generated here by the "make" step of contur installation, and will contain
the .ana files listing the rivet routines to be run.






