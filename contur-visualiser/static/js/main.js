var x, y, z, text, links;
var xlabel, ylabel;
$.ajax({
    type: "GET",
    async: false,
    url: '/collect_x',
    success: function(data) {
        x = data.split(";");
        for (var i = 0; i < x.length; i++) {
            x[i] = parseFloat(x[i]);
        }
    }
});
$.ajax({
    type: "GET",
    async: false,
    url: '/collect_y',
    success: function(data) {
        y = data.split(";");
        for (var i = 0; i < y.length; i++) {
            y[i] = parseFloat(y[i]);
        }
    }
});
$.ajax({
    type: "GET",
    url: '/collect_z',
    async: false,
    success: function(data) {
        z = data.split(";");
        for (var i = 0; i < z.length; i++) {
            z[i] = parseFloat(z[i]);
        }
    }
});
$.ajax({
    type: "GET",
    url: '/collect_text',
    async: false,
    success: function(data) {
        text = data.split(";");
    }
});
$.ajax({
    type: "GET",
    url: '/collect_links',
    async: false,
    success: function(data) {
        links = data.split(";");
    }
});

$.ajax({
    type: "GET",
    url: '/xlabel',
    async: false,
    success: function(data) {
        xlabel = data;
    }
});
$.ajax({
    type: "GET",
    url: '/ylabel',
    async: false,
    success: function(data) {
        ylabel = data;
    }
});

// You can use the click handler to implement this.But it may be hard to open in new tab, given most browsers
// try to prevent popups at all times.


var trace1 = {
    x: x,
    y: y,
    z: z,
    mode: 'markers',
    type: 'scatter3d',
    text: text,
    hoverinfo: 'x+y+z',
    marker: {
        size: 5
    }
};

var data = [trace1];

var layout = {
    title: 'Example Points',
    margin: {
        l: 0,
        r: 0,
        b: 0,
        t: 0
    },
    xaxis: {
        title: {
            text: xlabel
        }
    },
    yaxis: {
        title: {
            text: ylabel
        }
    },
    zaxis: {
        title: {
            text: 'z'
        }
    }
};

var myPlot = document.getElementById('scatter');
Plotly.newPlot(myPlot, data, layout);

var data = [{
    x: x,
    y: y,
    z: z,
    type: 'contour',
    line: {
        smoothing: 0.85
    },
    autocontour: false,
    contours: {
        start: 0.65,
        end: 0.80,
        size: 0.15,
        coloring: 'lines',
        showlabels: 'true',
        labelfont: {
            color: "black"
        }
    },
    text: text,
    hoverinfo: 'x+y+z'
}];

var layout = {
    title: 'Contour Plot',
    xaxis: {
        title: {
            text: xlabel
        }
    },
    yaxis: {
        title: {
            text: ylabel
        }
    },
    zaxis: {
        title: {
            text: 'z'
        }
    }
};

Plotly.newPlot('contour', data, layout);

var data = [{
    x: x,
    y: y,
    z: z,
    type: 'heatmap',
    line: {
        smoothing: 0.85
    },
    autocontour: false,
    contours: {
        start: 0.65,
        end: 0.80,
        size: 0.15,
        coloring: 'lines',
        showlabels: 'true',
        labelfont: {
            color: "black"
        }
    },
    text: text,
    hoverinfo: 'x+y+z'
}];

var layout = {
    title: 'Heatmap Plot',
    xaxis: {
        title: {
            text: xlabel
        }
    },
    yaxis: {
        title: {
            text: ylabel
        }
    },
    zaxis: {
        title: {
            text: 'z'
        }
    }
};

Plotly.newPlot('heatmap', data, layout);

function graphType() {

    var contour = document.getElementById("contour");
    var heatmap = document.getElementById("heatmap");
    var scatter = document.getElementById("scatter");
    var dropdown = document.getElementById("graph-point");
    var infodiv = document.getElementById('info');
    var slider = dropdown.options[dropdown.selectedIndex];
    infodiv.innerHTML = "";
    if (slider.value == 1) {
        scatter.style.display = "block";
        heatmap.style.display = "none";
        contour.style.display = "none";
    } else if (slider.value == 2) {
        scatter.style.display = "none";
        heatmap.style.display = "block";
        contour.style.display = "none";
    } else if (slider.value == 3) {
        scatter.style.display = "none";
        heatmap.style.display = "none";
        contour.style.display = "block";
    }

}

var contour = document.getElementById("contour");
var heatmap = document.getElementById("heatmap");
var scatter = document.getElementById("scatter");

scatter.on('plotly_click', function(data) {
    desc = data.points['0'].text;
    desc = desc.replace("#", "").split("<br>")[0].split(" ")[2].replace(/,\s*$/, "");;
    scatter.style.display = "none"
    $.ajax({
        type: "POST",
        async: false,
        data: desc,
        url: '/make_plots',
        success: function() {
            window.open("/index");
        },
        complete: function() {
            $("#loading-image").hide();
        }
    });
    scatter.style.display = "block"
})

scatter.on('plotly_hover', function(data) {
    var infodiv = document.getElementById('info');
    infodiv.innerHTML = data.points[0].text;
})

heatmap.on('plotly_click', function(data) {
    desc = data.points['0'].text;
    desc = desc.replace("#", "").split("<br>")[0].split(" ")[2].replace(/,\s*$/, "");;
    heatmap.style.display = "none"
    $.ajax({
        type: "POST",
        async: false,
        data: desc,
        url: '/make_plots',
        success: function() {
            window.open("/index");
        },
        complete: function() {
            $("#loading-image").hide();
        }
    });
    heatmap.style.display = "block"
})

heatmap.on('plotly_hover', function(data) {
    var infodiv = document.getElementById('info');
    infodiv.innerHTML = data.points[0].text;
})

contour.on('plotly_click', function(data) {
    desc = data.points['0'].text;
    desc = desc.replace("#", "").split("<br>")[0].split(" ")[2].replace(/,\s*$/, "");;
    contour.style.display = "none"
    $.ajax({
        type: "POST",
        async: false,
        data: desc,
        url: '/make_plots',
        success: function() {
            window.open("/index");
        },
        complete: function() {
            $("#loading-image").hide();
        }
    });
    contour.style.display = "block"
})

contour.on('plotly_hover', function(data) {
    var infodiv = document.getElementById('info');
    infodiv.innerHTML = data.points[0].text;
})