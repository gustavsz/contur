# Contur

Contur is a procedure and toolkit designed to set limits on theories Beyond the
Standard Model using measurements at particle colliders. The original procedure
is defined in a
[white paper](https://link.springer.com/article/10.1007%2FJHEP03%282017%29078)
, which should be used as a reference for this method. 

Results and more information on the method are available [here](https://hepcedar.gitlab.io/contur-webpage/index.html). This README 
concentrates on how to get it running.

## Directory structure

### AnalysisTools	
		
The main source code and template run area.

### Models			

Library of some of the models already used, plus some examples.

### TheoryRaw			

Archive of files (in various formats) from which SM theory predictions were imported.

### modified_analyses	

Staging area for rivet analyses which have been added or fixed since the last rivet release. The
`Theory` subdirectory contains the SM theory predictions we have, converted into yoda file format.


## Setting up Contur

Contur and Rivet are generator-independent, and so as long as you have an event generator producing
HepMC files, you can run them through Rivet and then run Contur on the resulting yoda file. However,
we currently run most BSM parameter scans using Herwig and this documentation assumes you want to do that too. 
If you have already got your generator running just want to run Contur on a HepMC file you have made, 
skip the bits involving Herwig marked with **.

To run Contur locally including parameter scans you will need to install 
[Herwig](https://herwig.hepforge.org/)**,
[Rivet](https://rivet.hepforge.org/) and [Yoda](https://yoda.hepforge.org/).
These can all be installed together using the Herwig bootstrap you can download
[here](https://herwig.hepforge.org/downloads.html), or you can install 
[Rivet](https://rivet.hepforge.org) and [Yoda](https://yoda.hepforge.org)
directly from their Hepforge pages.

You will also need a Python 2.7 environment.

Note that Dockerfiles containing a working setup (with or without Herwig) are available in the [docker subdirectory](docker). 
Instructions for how best to use Contur from these are still in preparation, but you can now get the latest by doing 
`docker pull hepstore/contur`. This is still a bit "beta", but the 
Rivet/Yoda docker setup is more mature and the guidance 
[here](https://gitlab.com/hepcedar/rivet/-/blob/release-3-1-x/doc/tutorials/docker.md) 
might help.

If you are running via docker, you can skip to the "Create Run Area" below.

#### Check Out Repository

- Check out code from repository. (Assume you start in $HOME.)

        $ git clone https://gitlab.com/hepcedar/contur.git

- This will get you the default version. Go into the contur directory
  that will have been created.

        $ cd contur/

- The default, recommended verion now runs with rivet 3, and is the version these instructions pertain to. If you need to go to a different version, just do

        $ git checkout <branch name or tag name>

  The recommended tag for running with rivet 3.1.1 is the latest in the contur-1.2.x series. The branch with minor fixes to this is the default (master). 
  There will likely also be feature development branches.
  If you want to use rivet 2, checkout the contur-0.0.1 tag. 
  If you want to use rivet 3.1.0, checkout the contur-1.1.1 tag.

#### Set Up Environment

- You need to make sure your environment is set up appropriately for the required version
  of Herwig, Rivet etc. If these are in $CEDARDIR/$INSTALLVER, for example:
  
        $ source $CEDARDIR/$INSTALLVER/rivetenv.sh
        $ source $CEDARDIR/$INSTALLVER/yodaenv.sh

- Set up environments; mostly adding relevant folders to the system path such
  that you can execute the contur commands from anywhere.
  You will then need to go into your contur directory and
  
        $ source setupContur.sh

- You need to do both the above steps every time you login. 


#### Build Analyses Databases
- Now in your contur directory type

        $ make

- You need only do this once. This will build any modified rivet analyses, build the database of static analysis information, 
  create a directory called GridPack with some analysis sterring files (*.ana) and create a script to define some envoronment 
  variables which can be used to run rivet on a HepMC file. After doing make, do

        $ source setupContur.sh	     

  again to execute this script and define the variables.

#### Create Run Area     
- Create a run area seperate from the repository where you installed Contur.
  This run area is where you will run everything from now on.

        $ mkdir run-area
        $ cd run-area
        $ cp -r  $CONTURMODULEDIR/AnalysisTools/GridSetup/GridPack .

#### Choose a Model

This section is specific to running Herwig with one of these models, a recommended first step.
Skip if you just want to run on an existing HepMC file.

- Choose a model. You should copy a UFO model directory from somewhere. Some of those previously used
in contur are in '$CONTURMODULEDIR/Models', for example DM_vector_mediator_UFO is the model used in the first contur paper.

        $ cd GridPack
        $ cp -r $CONTURMODULEDIR/Models/DM/DM_vector_mediator_UFO .

- Example Herwig `LHC-example.in` files are provided in the model directories. These
  usually specify parameters in curly brackets, e.g. `{name}`, for future use
  with the batch system. For a first single run, replace these expressions with some
  concrete numerical value. An example with this done for you is in `DM_vector_mediator_UFO/LHC-test.in`.
  (This test file also specifies beam energies and saves the configuration. For batch running, those lines
   are added automatically by the submission script.)

- Build the UFO model using Herwig's 'ufo2herwig' command.

        $ ufo2herwig DM_vector_mediator_UFO/
        $ make

#### Herwig and Rivet combined run on a Single Single Set of Analyses

This section is specific to a single run of Herwig with one of these models, a recommended first step.
Skip if you just want to run on an existing HepMC file.

- Copy the example LHC-test.in file from inside the model to the top level of your run area.
  
        $ cd run-area
        $ cp GridPack/DM_vector_mediator_UFO/LHC-test.in LHC.in

- Build the Herwig run card (LHC.run).

        $ Herwig read LHC.in -I GridPack -L GridPack

- Run the Herwig run card, specifying the number of events to generate. This
  can take a while so, as a first test, running around 200 events is fine.

        $ Herwig run LHC.run  -N 200

- This will produce the file LHC.yoda containing the results of the Herwig run.

#### Running rivet standalone on a HepMC file 

If you have produced a HepMC event file from somewhere and just want to run contur on that, this step applied. Skip
if you have run Herwig+Rivet together in the previous step.

All that is needed is 

        $ rivet -a $ANALYSIS-LIST myfile.hepmc

where $ANALYSIS-LIST is a comma-separated list of the analyses you want to run. Contur defines some convenient environment 
variables with the list of relevant analyses for 7, 8 and 13 TeV LHC running, $RA7TeV, $RA8TeV and $RA13TeV. 
Rivet will produce a .yoda file which you can then use in the following steps. For other rivet command line options, see

        $ rivet --help

as usual.

If you are running a generator independently of Rivet, you can use a pipe for this, so the (sometimes large) file never has to be resident on disk. For example:

        $ mkfifo fifo.hepmc
        $ run-pythia -n 200000 -e 8000 -c Top:all=on -o fifo.hepmc &
        $ rivet fifo.hepmc -a $RA8TeV

If you have a version of Herwig built without the rivet interface, you need to remove all the rivet references from
the .in file (and do not include a .ana file), and add the instructions to tell Herwig to write out a HepMC file

        read snippets/HepMC.in
        set /Herwig/Analysis/HepMC:PrintEvent 10000000

This is done for you in the `LHC-pipe-test.in` files in the DM_vector_mediator_UFO directory.

#### Running Contur on a single yoda file

Once you have produced a yoda file from rivet, this step is applicable however you produced it.

- Analyse a Yoda file created by one of the procedures above with contur. (Run 'contur --help' to see more options).

        $ contur LHC.yoda 

- The contur script will output a plots folder and an ANALYSIS folder and
  print some other information.

- Generate 'contur-plots' directory containing histograms and an index.html
  file to view them all. Whilst still in the GridPack directory, run:

        $ contur-mkhtml LHC.yoda

#### Running a Batch Job to Generate Heatmaps (TODO)

- Go to base directory of copied over run area.

        $ cd run-area
        
- Now copy over the example for batch running:

        $ cp GridPack/DM_vector_mediator_UFO/LHC-example.in LHC.in

- Define a parameter space in 'param_file.dat'. You can copy the example:

        $ cp GridPack/DM_vector_mediator_UFO/param_file.dat .
        
- It should be a file formatted like this:

```
 #Based on configObj python template
[Run]
generator ='[path to my environment setup]setupEnv.sh'
contur ='[path to my contur directory]setupContur.sh'

[Parameters]
[[mXm]]
mode = LIN
start = 10.0
stop = 1610.0
number = 16
[[mY1]]
mode = LIN
start = 10.0
stop =  3610.0
number = 18
[[gYXm]]
mode = CONST
value = 1.0
[[gYq]]
mode = CONST
value = 0.25

```
- `setupEnv.sh` should be a script which sets up your runtime environment which the batch jobs will use. As a minimum
   is will need to contain the lines to execute your rivetenv.sh and yodaenv.sh files.

- `setupContur.sh` is the contur setup script in your contur directory.

- For both these set up files, you should give the full explicit path.

- You should check that the parameters defined are also defined at the top of
  the LHC.in file within the same directory.

- The example model has parameters 'Xm', 'Y1', 'gYXm', 'gYq', defined in
  'params_file.dat' and the LHC-example.in file has, at the top of the file:

        read FRModel.model
        set /Herwig/FRModel/Particles/Y1:NominalMass {Y1}
        set /Herwig/FRModel/Particles/Xm:NominalMass {Xm}
        set /Herwig/FRModel/FRModel:gYXm {gYXm}
        set /Herwig/FRModel/FRModel:gYq  {gYq}	 

  If you wanted to add or remove parameters you must do this in both files. For other models, different
  parameters are available/required.
  
- Run a test scan over the parameter space defined in 'param_file.dat' without submitting it
  to a batch farm.  (The `-s` flag ensure no jobs will be submitted.)

         $ contur-batch -n 1000 --seed 101 -s

  or if you have a version of Herwig without the rivet interface installed,use the pipe option:

         $ contur-batch -n 1000 --seed 101 -s -P

  This will produce a directory called 'myscan00' containing one directory for each known beam energy, each containing
  however many runpoint directories are indicated by the ranges in your param_file.dat. Have a look at the shell scripts 
  ('runpoint_xxxx.sh') which have been generated and the LHC.in files to check all is as you expected. 
  You can manually submit some of the 'runpoint_xxxx.sh' files as a test, or run Herwig local as above using the generated 
  LHC.in files.

- Now you are ready to run batch jobs. Remove the myscan00 directory tree you just created, and run the 
  batch submit command again, now without the `-s` flag and specifying the queue
  on your batch farm. For example:

         $ contur-batch -n 1000 --seed 101 -q mediumc7
  or
         $ contur-batch -n 1000 --seed 101 -P -q mediumc7

  
  (Note that we assume `qsub` is available on your system here. Slurm and condor batch systems are also supported. 
  If you have a different submission system you'll need to
  look into `$CONTURMODULEDIR/AnalysisTools/contur/contur/Scan/batch_submit.py` and work out how to change the appropriate submission
  commands.)

- A successful run will produce a directory called 'myscan00' as before. You need to wait for the farm
  to finish the jobs before continuing. You can check the progress using the 'qstat' command.

- When the batch job is complete there should, in every run point directory, be
  files 'LHC-runpoint_xxx.yoda'.

- Analyse results with contur. Resulting .map file will be output to the
  ANALYSIS folder.

        $ contur -g myscan00/

  For various options see

        $ contur --help

- Plot a heatmap.

        $ cd ANALYSIS/
        $ contur-plot --help
        $ contur-plot xxxx.map mXm mY1  -T "My First Heatmap"

#### Running Contur with Madgraph
Instead of Herwig, Contur can also be used in combination with other Monte Carlo generators that provide hepmc files. For Madgraph, it is recommended to use version 2.7.2 or later and the latest Rivet release (>3.1.2). When running

        $ $MG_DIR/bin/mg5_aMC <Madgraph script>

where `$MG_DIR` is the directory containg the Madgraph installation, Madgraph will produce an LHE file containing MC events in e.g. mgevents/Events/Run01. Include `shower=Pythia8` in your Madgraph script to have Pythia shower the events in the LHE file and give a hepmc file as output. This one can be read in with Rivet, giving the yoda file. As Madgraph provides a large number of event weights, it is recommended to use the `--skip-weights` option with Rivet to reduce the number of processed weights with different names.

        $ rivet --skip-weights -a $ANALYSIS-LIST <hepmc file>
        
For the same reason, when running Contur on the yoda file, use the `--wn "Weight_MERGING=0.000"` option

        $ contur --wn "Weight_MERGING=0.000" <yoda file>

When using `contur-batch`, set the generator name to Madgraph using `-m`:

        $ contur-batch -m madgraph -p <param file> -t <Madgraph script>

Within the param file, make sure the model parameters are set similar to the the way it's done for Herwig, e.g.

        set gPXd        {gPXd}
        set tanbeta     {tanbeta}

By default, Madgraph runs on multiple cores. On a batch system, the jobs are however usually assigned a single core. To configure Madgraph correctly for single core mode and thus use the computational ressources most efficiently, include

        set run_mode 0
        set nb_core 1
        set low_mem_multicore_nlo_generation

in your Madgraph script. An example Madgraph script containing the important functionality can be found at Models/DM/Pseudoscalar_2HDM/mg-example.sh.

As Madgraph might generate a large quantity of output while running, it is recommended to run the Contur clean-up functionality

        $ contur-gridtool -r <grid directory>

directly after all jobs have finished.

#### Other functionality

`contur-gridtool` provides some utilities for compressing or merging grids and recovering failed grid points.

