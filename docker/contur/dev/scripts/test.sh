set -e
cd /contur
mkdir run-area
cd run-area
cp -r  $CONTURMODULEDIR/AnalysisTools/GridSetup/GridPack .
cd GridPack
cp -r $CONTURMODULEDIR/Models/DM/DM_vector_mediator_UFO .
python2 $(which ufo2herwig) DM_vector_mediator_UFO/
make
cd ../
cp GridPack/DM_vector_mediator_UFO/LHC-test.in LHC.in
Herwig read LHC.in -I GridPack -L GridPack
Herwig run LHC.run  -N 10
cp GridPack/DM_vector_mediator_UFO/LHC-example.in LHC.in
cp GridPack/DM_vector_mediator_UFO/param_file.dat .