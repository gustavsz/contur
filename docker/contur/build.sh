#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.2
CONTUR_VERSION=master #1.2.0

MSG="Building contur with Rivet=$RIVET_VERSION"
tag="hepstore/contur:$CONTUR_VERSION"

docker build . -f Dockerfile -t $tag

docker tag $tag hepstore/contur:$CONTUR_VERSION
docker tag $tag hepstore/contur:latest

if [[ "$PUSH" = 1 ]]; then
    docker push $tag && sleep 30s
    docker push hepstore/contur:$CONTUR_VERSION && sleep 30s
    docker push hepstore/contur:latest
fi
